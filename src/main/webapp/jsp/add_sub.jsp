<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:bundle basename="pagetext" prefix="add_sub.">
	<html>
<head>
<link href="css/bootstrap.css" rel="stylesheet">
<title><fmt:message key="label.title" /></title>
</head>
<body>
	<ctg:roletag role="${ user.role }" />
	<c:import url="taghead\header.jsp" />
	<c:import url="common\logo.jsp" />
	<c:import url="tagmenu\head_menu.jsp" />
	<div class="main text-main">
		<h4 align="center">
			<fmt:message key="message.checkBook" />
		</h4>
		<p align="center">
			<fmt:message key="message.name" />
			${ exemplar.book.name }<br />
			<fmt:message key="message.author" />
			${ exemplar.book.authorFistName } ${ exemplar.book.authorLastName }
		</p>
	</div>
	<br />
	<c:if test="${ admin }">
		<c:if test="${ !createSuccess }">
			<form name="addSubForm" method="POST" action="controller"
				class="form">
				<input type="hidden" name="command" value="create_subscription" />
				<input type="hidden" name="old_exemplar_status"
					value="${ exemplar.exemplarStatus }" /> <input type="hidden"
					name="exemplar_id" value="${ exemplar.id }" /> <label
					for="genre_id"><fmt:message key="message.chooseUser" /></label> <select
					class="form-control" name="user_id">
					<c:forEach var="user" items="${userInfo}" varStatus="status">
						<option value="${ user.id }"><c:out
								value="${ user.firstName }" />
							<c:out value="${ user.lastName }" /></option>
					</c:forEach>
				</select>
				<div class="form-group">
					<label for="name"><fmt:message key="message.endDate" /></label> <input
						type="date" name="end_date" class="form-control"
						required="required">
				</div>
				<input type="hidden" name="load" value="sub_info"><small><fmt:message
						key="help.required" /></small><input type="submit"
					class="btn btn-primary btn-block"
					value="<fmt:message key="button.submit" />" />
			</form>
		</c:if>
	</c:if>
	<c:if test="${ reader }">
		<div class="main text-main">
			<h4 align="center">
				<fmt:message key="message.checkUser" />
			</h4>
			<p align="center">
				<fmt:message key="message.user" />
				${ user.firstName } ${ user.lastName }<br />
				<fmt:message key="message.userEndDate" />
			</p>
		</div>
		<br />
		<c:if test="${!createSuccess}">
			<form name="addSubForm" method="POST" action="controller"
				class="form">
				<input type="hidden" name="command" value="create_subscription" />
				<input type="hidden" name="old_exemplar_status"
					value="${ exemplar.exemplarStatus }" /> <input type="hidden"
					name="exemplar_id" value="${ exemplar.id }" /> <input
					type="hidden" name="user_id" value="${ user.id }" /> <input
					type="hidden" name="load" value="sub_info"><input
					type="submit" class="btn btn-primary btn-block"
					value="<fmt:message key="button.submit" />" />
			</form>
		</c:if>
	</c:if>
	<c:if test="${errorCreateMessage}">
		<br />
		<div class="alert alert-danger">
			<fmt:message key="error.createError" />
		</div>
	</c:if>
	<c:if test="${hasOverdueError}">
		<br />
		<c:choose>
			<c:when test="${ admin }">
				<div class="alert alert-danger">
					<fmt:message key="error.hasOverdueErrorAdmin" />
				</div>
			</c:when>
			<c:otherwise>
				<div class="alert alert-danger">
					<fmt:message key="error.hasOverdueErrorUser" />
				</div>
			</c:otherwise>
		</c:choose>
	</c:if>
	<c:if test="${exemplarStatusNotValid}">
		<br />
		<div class="alert alert-danger">
			<fmt:message key="error.exemplarStatusNotValid" />
		</div>
	</c:if>
	<c:if test="${endSubscriptionDateNotValid}">
		<br />
		<div class="alert alert-danger">
			<fmt:message key="error.endSubscriptionDateNotValid" />
		</div>
	</c:if>
	<c:if test="${createSuccess}">
		<br />
		<div class="alert alert-success">
			<fmt:message key="message.creationSuccess" />
			<c:if test="${ reader }">
				<fmt:message key="message.successReaderMsg" />
			</c:if>
		</div>
	</c:if>
	<c:import url="common\line.jsp" />
	<ctg:footer />
</body>
	</html>
</fmt:bundle>