<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:bundle basename="pagetext" prefix="exemplars.">
	<html>
<head>
<link href="css/bootstrap.css" rel="stylesheet">
<title><fmt:message key="label.title" /></title>
</head>
<body>
	<ctg:roletag role="${ user.role }" />
	<c:import url="taghead\header.jsp" />
	<c:import url="common\logo.jsp" />
	<c:import url="tagmenu\head_menu.jsp" />
	<c:choose>
		<c:when test="${ loadSuccess }">
			<div class="main text-main">
				<c:choose>
					<c:when test="${ admin }">
						<fmt:message key="message.adminHead" />
					</c:when>
					<c:otherwise>
						<fmt:message key="message.readerHead" />
					</c:otherwise>
				</c:choose>
			</div>
			<br />
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<c:choose>
								<c:when test="${ admin }">
									<th><fmt:message key="table.delete" /></th>
									<th><fmt:message key="table.addSub" /></th>
								</c:when>
								<c:otherwise>
									<th><fmt:message key="table.take" /></th>
								</c:otherwise>
							</c:choose>
							<th><fmt:message key="table.name" /></th>
							<th><fmt:message key="table.authorName" /></th>
							<th><fmt:message key="table.edition" /></th>
							<th><fmt:message key="table.genre" /></th>
							<th><fmt:message key="table.status" /></th>
						</tr>
					</thead>
					<c:forEach var="exemplar" items="${ exemplars }" varStatus="status">
						<tbody>
							<tr>
								<c:choose>
									<c:when test="${ admin }">
										<td align="center" valign="middle"><form method="POST"
												action="controller">
												<input type="hidden" name="command" value="delete_exemplar" />
												<input type="hidden" name="exemplar_id"
													value="${ exemplar.id }"><input type="hidden"
													name="load" value="exemplar"><input type="hidden"
													name="status" value="${ exemplar.exemplarStatus }"><input
													type="hidden" name="book_id" value="${ exemplar.book.id }">
												<input type="image" src="img/icons/del_book.png"
													title="<fmt:message key="button.titleDel" />" />
											</form></td>
										<td align="center" valign="middle"><form method="POST"
												action="controller">
												<input type="hidden" name="command" value="page" /> <input
													type="hidden" name="page" value="add_sub" /> <input
													type="hidden" name="load" value="sub_info" /> <input
													type="hidden" name="exemplar_id" value="${ exemplar.id }"><input
													type="hidden" name="book_id" value="${ exemplar.book.id }">
												<input type="image" src="img/icons/add_sub.png"
													title="<fmt:message key="button.title" />" />
											</form></td>
									</c:when>
									<c:otherwise>
										<td align="center" valign="middle"><form
												name="buttomForm" method="POST" action="controller">
												<input type="hidden" name="command" value="page" /> <input
													type="hidden" name="page" value="add_sub" /> <input
													type="hidden" name="load" value="sub_info" /><input
													type="image" src="img/icons/add_sub.png" /> <input
													type="hidden" name="exemplar_id" value="${ exemplar.id }">
												<input type="hidden" name="book_id"
													value="${ exemplar.book.id }">
											</form></td>
									</c:otherwise>
								</c:choose>
								<td><c:out value="${ exemplar.book.name }" /></td>
								<td><c:out value="${ exemplar.book.authorFistName }" /> <c:out
										value="${ exemplar.book.authorLastName }" /></td>
								<td><c:out value="${ exemplar.book.publisher }" /></td>
								<td><c:out value="${ exemplar.book.genre.subgenre }" /></td>
								<td><c:choose>
										<c:when test="${'FREE' == exemplar.exemplarStatus }">
											<fmt:message key="table.free" />
										</c:when>
										<c:otherwise>
											<fmt:message key="table.taken" />
										</c:otherwise>
									</c:choose></td>
							</tr>
						</tbody>
					</c:forEach>
				</table>
			</div>
		</c:when>
		<c:when test="${ exemplarsEmpty }">
			<div class="alert alert-warning">
				<fmt:message key="message.empty" />
			</div>
		</c:when>
		<c:when test="${ loadExemplarsError }">
			<div class="alert alert-danger">
				<fmt:message key="message.error" />
			</div>
		</c:when>
	</c:choose>
	<c:if test="${ deleteSuccess }">
		<br />
		<div class="alert alert-success">
			<fmt:message key="message.deleteSuccess" />
		</div>
	</c:if>
	<c:if test="${hasSubscriptionsError}">
		<br />
		<div class="alert alert-danger">
			<fmt:message key="message.hasSubscriptionsError" />
		</div>
	</c:if>
	<c:if test="${ errorDeleteMessage }">
		<br />
		<div class="alert alert-danger">
			<fmt:message key="message.deleteError" />
		</div>
	</c:if>
	<c:import url="common\line.jsp" />
	<ctg:footer />
</body>
	</html>
</fmt:bundle>