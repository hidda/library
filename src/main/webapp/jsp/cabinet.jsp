<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:bundle basename="pagetext" prefix="cabinet.">
	<html>
<head>
<link href="css/bootstrap.css" rel="stylesheet">
<title><fmt:message key="label.title" /></title>
</head>
<body>
	<c:import url="taghead\header.jsp" />
	<c:import url="common\logo.jsp" />
	<c:import url="tagmenu\head_menu.jsp" />
	<div class="main" align="center">
		<form name="buttomForm" method="POST" action="controller"
			class="button-form">
			<input type="hidden" name="command" value="delete_user" /> <input
				type="hidden" name="user_id" value="${ user.id }" /> <input
				type="image" src="<fmt:message key="button.deleteProfile"/>" />
		</form>
		<form name="buttomForm" method="POST" action="controller"
			class="button-form">
			<input type="hidden" name="command" value="page" /> <input
				type="hidden" name="page" value="edit_profile" /><input
				type="image" src="<fmt:message key="button.editProfile"/>" />
		</form>
	</div>
	<c:import url="common\line.jsp" />
	<c:if test="${registrationSuccess}">
		<div class="alert alert-success">
			<fmt:message key="message.registrationSuccess" />
		</div>
		<c:import url="common\line.jsp" />
	</c:if>
	<div class="main text-main">
		<label><fmt:message key="message.welcome" />${ user.firstName }
			${ user.lastName }</label><br />
		<fmt:message key="message.mainText" />
	</div>
	<br />
	<c:if test="${ reader }">
		<c:choose>
			<c:when test="${ loadSuccess }">
				<div class="main text-main">
					<fmt:message key="message.readerHead" />
				</div>
				<br />
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th><fmt:message key="table.bookName" /></th>
								<th><fmt:message key="table.bookAuthor" /></th>
								<th><fmt:message key="table.startDate" /></th>
								<th><fmt:message key="table.endDate" /></th>
								<th><fmt:message key="table.overdue" /></th>
								<th><fmt:message key="table.closed" /></th>
							</tr>
						</thead>
						<c:forEach var="sub" items="${subscriptions}" varStatus="status">
							<tbody>
								<c:choose>
									<c:when test="${ sub.overdue }">
										<tr class="danger">
											<td><c:out value="${ sub.exemplar.book.name}" /></td>
											<td><c:out value="${ sub.exemplar.book.authorFistName }" />
												<c:out value="${ sub.exemplar.book.authorLastName }" /></td>
											<td><c:out value="${ sub.startSubcriptionDate }" /></td>
											<td><c:out value="${ sub.endSubcriptionDate }" /></td>
											<td><fmt:message key="table.isOverdue" /></td>
											<td><fmt:message key="table.nonClosed" /></td>
										</tr>
									</c:when>
									<c:when test="${ sub.closed }">
										<tr class="success">
											<td><c:out value="${ sub.exemplar.book.name}" /></td>
											<td><c:out value="${ sub.exemplar.book.authorFistName }" />
												<c:out value="${ sub.exemplar.book.authorLastName }" /></td>
											<td><c:out value="${ sub.startSubcriptionDate }" /></td>
											<td><c:out value="${ sub.endSubcriptionDate }" /></td>
											<td><fmt:message key="table.nonOverdue" /></td>
											<td><fmt:message key="table.isClosed" /></td>
										</tr>
									</c:when>
									<c:otherwise>
										<tr>
											<td><c:out value="${ sub.exemplar.book.name}" /></td>
											<td><c:out value="${ sub.exemplar.book.authorFistName }" />
												<c:out value="${ sub.exemplar.book.authorLastName }" /></td>
											<td><c:out value="${ sub.startSubcriptionDate }" /></td>
											<td><c:out value="${ sub.endSubcriptionDate }" /></td>
											<td><fmt:message key="table.nonOverdue" /></td>
											<td><fmt:message key="table.nonClosed" /></td>
										</tr>
									</c:otherwise>
								</c:choose>
							</tbody>
						</c:forEach>
					</table>
				</div>
			</c:when>
			<c:when test="${subscriptionsEmpty}">
				<div class="alert alert-warning">
					<fmt:message key="message.empty" />
				</div>
			</c:when>
			<c:when test="${loadSubscriptionsError}">
				<div class="alert alert-danger">
					<fmt:message key="message.error" />
				</div>
			</c:when>
		</c:choose>
	</c:if>
	<c:if test="${hasSubscriptionsError}">
		<div class="alert alert-danger">
			<fmt:message key="message.hasSubscriptionsError" />
		</div>
	</c:if>
	<c:if test="${errorDeleteMessage}">
		<div class="alert alert-danger">
			<fmt:message key="message.errorDelete" />
		</div>
	</c:if>
	<c:import url="common\line.jsp" />
	<ctg:footer />
</body>
	</html>
</fmt:bundle>