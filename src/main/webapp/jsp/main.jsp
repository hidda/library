<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:setLocale value="${ locale }" scope="session" />
<fmt:bundle basename="pagetext" prefix="main.">
	<html>
<head>
<link href="css/bootstrap.css" rel="stylesheet">
<title><fmt:message key="label.title" /></title>
</head>
<body>
	<ctg:roletag role="${ user.role }" />
	<c:import url="taghead\header.jsp" />
	<c:import url="common\logo.jsp" />
	<c:import url="tagmenu\head_menu.jsp" />
	<div class="main text-main">
		<c:choose>
			<c:when test="${ admin }">
				<fmt:message key="message.mainAdmin" />
			</c:when>
			<c:when test="${ reader }">
				<fmt:message key="message.mainReader" />
			</c:when>
			<c:otherwise>
				<fmt:message key="message.mainGuest" />
			</c:otherwise>
		</c:choose>
	</div>
	<c:import url="common\line.jsp" />
	<ctg:footer />
</body>
	</html>
</fmt:bundle>