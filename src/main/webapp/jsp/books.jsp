<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:bundle basename="pagetext" prefix="books.">
	<html>
<head>
<link href="css/bootstrap.css" rel="stylesheet">
<title><fmt:message key="label.title" /></title>
</head>
<body>
	<ctg:roletag role="${ user.role }" />
	<c:import url="taghead\header.jsp" />
	<c:import url="common\logo.jsp" />
	<c:import url="tagmenu\head_menu.jsp" />
	<br />
	<div class="main" align="right">
		<form class="form-inline" role="form" method="POST"
			action="controller">
			<input type="hidden" name="command" value="page" /><input
				type="hidden" name="page" value="books" /><input type="hidden"
				name="load" value="genre_books" /><input type="hidden" name="load"
				value="genre" />
			<div class="form-group">
				<label for="genre"><fmt:message key="label.find" /></label> <select
					class="form-control" name="genre_id">
					<c:forEach var="genre" items="${genres}" varStatus="status">
						<option value="${ genre.id }"><c:out
								value="${ genre.subgenre }" /></option>
					</c:forEach>
				</select>
			</div>
			<input style="vertical-align: middle" type="image"
				src="<fmt:message key="button.search"/>" />
		</form>
	</div>
	<c:choose>
		<c:when test="${ loadSuccess }">
			<div class="main text-main">
				<c:choose>
					<c:when test="${ admin }">
						<fmt:message key="message.adminHead" />
					</c:when>
					<c:otherwise>
						<fmt:message key="message.readerHead" />
					</c:otherwise>
				</c:choose>
			</div>
			<br />
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<c:if test="${ admin }">
								<th><fmt:message key="table.delete" /></th>
							</c:if>
							<th><fmt:message key="table.name" /></th>
							<th><fmt:message key="table.authorName" /></th>
							<th><fmt:message key="table.edition" /></th>
							<th><fmt:message key="table.year" /></th>
							<th><fmt:message key="table.amount" /></th>
							<th><fmt:message key="table.genre" /></th>
						</tr>
					</thead>
					<c:forEach var="book" items="${books}" varStatus="status">
						<tbody>
							<tr>
								<c:if test="${ admin }">
									<td align="center" valign="middle"><form name="buttomForm"
											method="POST" action="controller">
											<input type="hidden" name="command" value="delete_book" /><input
												type="hidden" name="book_id" value="${book.id}" /><input
												type="hidden" name="load" value="book"><input
												type="hidden" name="load" value="genre"><input
												type="image" src="img/icons/del_book.png"
												title="<fmt:message key="button.title" />" />
										</form></td>
								</c:if>
								<td><a
									href="controller?page=exemplars&load=book_exemplars&book_id=${book.id}&locale=${ locale }"><c:out
											value="${ book.name }" /></a></td>
								<td><c:out value="${ book.authorFistName }" /> <c:out
										value="${ book.authorLastName }" /></td>
								<td><c:out value="${ book.publisher }" /></td>
								<td><c:out value="${ book.year }" /></td>
								<td><c:out value="${ book.amount }" /></td>
								<td><c:out value="${ book.genre.subgenre }" /></td>
							</tr>
						</tbody>
					</c:forEach>
				</table>
			</div>
		</c:when>
		<c:when test="${booksEmpty}">
			<div class="alert alert-warning">
				<fmt:message key="message.empty" />
			</div>
		</c:when>
		<c:when test="${loadBooksError}">
			<div class="alert alert-danger">
				<fmt:message key="message.error" />
			</div>
		</c:when>
	</c:choose>
	<c:if test="${ deleteSuccess }">
		<br />
		<div class="alert alert-success">
			<fmt:message key="message.deleteSuccess" />
		</div>
	</c:if>
	<c:if test="${hasSubscriptionsError}">
		<br />
		<div class="alert alert-danger">
			<fmt:message key="message.hasSubscriptionsError" />
		</div>
	</c:if>
	<c:if test="${ errorDeleteMessage }">
		<br />
		<div class="alert alert-danger">
			<fmt:message key="message.deleteError" />
		</div>
	</c:if>
	<c:import url="common\line.jsp" />
	<ctg:footer />
</body>
	</html>
</fmt:bundle>