<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:bundle basename="pagetext" prefix="registration.">
	<html>
<head>
<link href="css/bootstrap.css" rel="stylesheet">
<script type="text/javascript" src="js/passwordcheck.js"></script>
<title><fmt:message key="label.title" /></title>
</head>
<body>
	<c:import url="common\logo.jsp" />
	<form name="loginForm" method="POST" action="controller" class="form">
		<input type="hidden" name="command" value="create_user" />
		<div class="form-group">
			<label for="login"><fmt:message key="message.login" /></label><br />
			<small><fmt:message key="help.login" /></small> <input type="text"
				name="login" value="${ login }" pattern="[A-Za-z_]{6,}"
				title="<fmt:message key="help.login" />" class="form-control"
				required="required">
		</div>
		<div class="form-group">
			<label for="password"><fmt:message key="message.password" /></label><br />
			<small><fmt:message key="help.password" /></small> <input
				type="password" id="password1" class="form-control" name="password"
				pattern="[\wА-ЯЁа-яё]{8,}"
				title="<fmt:message key="help.password" />" required="required">
		</div>
		<div class="form-group">
			<label for="password"><fmt:message
					key="message.confirmPassword" /></label> <input type="password"
				id="password2" class="form-control" required="required">
		</div>
		<div class="form-group">
			<label for="first_name"><fmt:message key="message.firstName" /></label>
			<input type="text" name="first_name" value="${ firstName }"
				pattern="[А-ЯЁ][А-ЯЁа-яё\s.-]+|[A-Z][\w\s.-]+"
				title="<fmt:message key="help.firstName" />" class="form-control"
				required="required">
		</div>
		<div class="form-group">
			<label for="last_name"><fmt:message key="message.lastName" /></label>
			<input type="text" name="last_name" value="${ lastName }"
				pattern="[А-ЯЁ][А-ЯЁа-яё\s.-]+|[A-Z][\w\s.-]+"
				title="<fmt:message key="help.lastName" />" class="form-control"
				required="required">
		</div>
		<div class="form-group">
			<label for="email"><fmt:message key="message.email" /></label> <input
				type="email" name="email" value="${ email }" class="form-control"
				required="required">
		</div>
		<small><fmt:message key="help.required" /></small> <input
			type="submit" class="btn btn-primary btn-block"
			value="<fmt:message key="button.submit" />" />
	</form>
	<c:if test="${errorCreateMessage}">
		<br />
		<div class="alert alert-danger">
			<fmt:message key="error.createError" />
		</div>
	</c:if>
	<c:if test="${loginNotUnique}">
		<br />
		<div class="alert alert-warning">
			<fmt:message key="error.loginNotUnique" />
		</div>
	</c:if>
	<c:if test="${validationError}">
		<br />
		<div class="alert alert-danger">
			<fmt:message key="error.validationError" />
		</div>
	</c:if>
	<c:import url="common\line.jsp" />
	<ctg:footer />
</body>
	</html>
</fmt:bundle>