<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<html>
<head>
<link href="css/bootstrap.css" rel="stylesheet">
</head>
<body>
	<ctg:roletag role="${ user.role }" />
	<c:choose>
		<c:when test="${admin}">
			<c:import url="tagmenu\admin_menu.jsp" />
			<c:import url="common\line.jsp" />
		</c:when>
		<c:when test="${reader}">
			<c:import url="tagmenu\reader_menu.jsp" />
			<c:import url="common\line.jsp" />
		</c:when>
	</c:choose>
</body>
</html>