<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:bundle basename="pagetext" prefix="menu.">
	<html>
<head>
<link href="css/bootstrap.css" rel="stylesheet">
</head>
<body>
	<div class="main" align="right">
		<form name="buttomForm" method="POST" action="controller"
			class="button-form">
			<input type="hidden" name="command" value="page" /> <input
				type="hidden" name="page" value="cabinet" /><input type="hidden"
				name="load" value="user_subscriptions" /> <input type="hidden"
				name="user_id" value="${ user.id }" /> <input type="image"
				src="<fmt:message key="button.cabinet"/>" />
		</form>
		<form name="buttomForm" method="POST" action="controller"
			class="button-form">
			<input type="hidden" name="command" value="page" /> <input
				type="hidden" name="page" value="books" /><input type="hidden"
				name="load" value="book" /><input type="hidden"
				name="load" value="genre" /> <input type="image"
				src="<fmt:message key="button.showBooks"/>" />
		</form>
		<form name="buttomForm" method="POST" action="controller"
			class="button-form">
			<input type="hidden" name="command" value="page" /> <input
				type="hidden" name="page" value="subscriptions" /> <input
				type="hidden" name="load" value="user_subscriptions" /> <input
				type="hidden" name="user_id" value="${ user.id }" /> <input
				type="image" src="<fmt:message key="button.showUserSub"/>" />
		</form>
	</div>
</body>
	</html>
</fmt:bundle>