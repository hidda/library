<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:bundle basename="pagetext" prefix="add_book.">
	<html>
<head>
<link href="css/bootstrap.css" rel="stylesheet">
<title><fmt:message key="label.title" /></title>
</head>
<body>
	<c:import url="taghead\header.jsp" />
	<c:import url="common\logo.jsp" />
	<c:import url="tagmenu\head_menu.jsp" />
	<div class="main text-main">
		<fmt:message key="message.headText" />
	</div>
	<br />
	<form name="addBookForm" method="POST" action="controller" class="form">
		<input type="hidden" name="command" value="create_book" />
		<div class="form-group">
			<label for="name"><fmt:message key="message.name" /></label><br />
			<small><fmt:message key="help.name" /></small><input type="text"
				name="name" value="${ name }"
				pattern="[А-ЯЁA-Z][\s\w\dА-ЯЁа-яё.,!?-]+|[\d][А-Яёа-яё\s\w!?.,-]+"
				title="<fmt:message key="help.name" />" class="form-control"
				required="required">
		</div>
		<div class="form-group">
			<label for="first_name"><fmt:message key="message.firstName" /></label>
			<input type="text" name="first_name" value="${ firstName }"
				pattern="[А-ЯЁ][А-ЯЁа-яё\s.-]+|[A-Z][\w\s.-]+"
				title="<fmt:message key="help.firstName" />" class="form-control"
				required="required">
		</div>
		<div class="form-group">
			<label for="last_name"><fmt:message key="message.lastName" /></label>
			<input type="text" name="last_name" value="${ lastName }"
				pattern="[А-ЯЁ][А-ЯЁа-яё\s.-]+|[A-Z][\w\s.-]+"
				title="<fmt:message key="help.lastName" />" class="form-control"
				required="required">
		</div>
		<div class="form-group">
			<label for="edition"><fmt:message key="message.edition" /></label><br />
			<small><fmt:message key="help.edition" /></small><input type="text"
				name="publisher" value="${ publisher }"
				pattern="[А-ЯЁ][А-ЯЁа-яё\s\d№.();:!?,-]+|[A-Z][\w\s\d№.,();:!?-]+"
				title="<fmt:message key="help.edition" />" class="form-control"
				required="required">
		</div>
		<div class="form-group">
			<label for="year"><fmt:message key="message.year" /></label> <input
				type="text" name="year" value="${ year }" pattern="\d{4}"
				title="<fmt:message key="help.year" />" class="form-control"
				required="required">
		</div>
		<div class="form-group">
			<label for="amount"><fmt:message key="message.amount" /></label> <input
				type="number" name="amount" value="${ amount }" class="form-control"
				required="required">
		</div>
		<label for="genre_id"><fmt:message key="message.genre" /></label> <select
			class="form-control" name="genre_id">
			<c:forEach var="genre" items="${genres}" varStatus="status">
				<option value="${ genre.id }"><c:out
						value="${ genre.subgenre }" /></option>
			</c:forEach>
		</select> <br /> <small><fmt:message key="help.required" /></small> <input
			type="hidden" name="load" value="genre"><input type="submit"
			class="btn btn-primary btn-block"
			value="<fmt:message key="button.submit" />" />
	</form>
	<c:if test="${errorCreateMessage}">
		<br />
		<div class="alert alert-danger">
			<fmt:message key="error.createError" />
		</div>
	</c:if>
	<c:if test="${createSuccess}">
		<br />
		<div class="alert alert-success">
			<fmt:message key="message.creationSuccess" />
		</div>
	</c:if>
	<c:if test="${validationError}">
		<br />
		<div class="alert alert-danger">
			<fmt:message key="error.validationError" />
		</div>
	</c:if>
	<c:import url="common\line.jsp" />
	<ctg:footer />
</body>
	</html>
</fmt:bundle>