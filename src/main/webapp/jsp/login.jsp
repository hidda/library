<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:bundle basename="pagetext" prefix="login.">
	<html>
<head>
<link href="css/bootstrap.css" rel="stylesheet">
<title><fmt:message key="label.title" /></title>
</head>
<body>
	<c:import url="common\logo.jsp" />
	<form name="loginForm" method="POST" action="controller" class="form">
		<input type="hidden" name="command" value="login" />
		<div class="form-group">
			<label for="login"><fmt:message key="message.login" /></label> <input
				type="text" name="login" value="${ login }" class="form-control"
				placeholder="<fmt:message key="message.login" />">
		</div>
		<div class="form-group">
			<label for="password"><fmt:message key="message.password" /></label>
			<input type="password" class="form-control" name="password"
				placeholder="<fmt:message key="message.password" />">
		</div>
		<br />
		<div class="main">
			<label for="lang"><fmt:message key="message.lang" /></label> <label
				class="radio-inline"><input type="radio"
				id="inlineCheckbox1" name="locale" value="ru_RU" checked="checked">
				<fmt:message key="label.langru" /></label> <label class="radio-inline">
				<input type="radio" id="inlineCheckbox2" name="locale" value="en_US">
				<fmt:message key="label.langen" />
			</label>
		</div>
		<br /> <input type="submit" class="btn btn-primary btn-block"
			value="<fmt:message key="button.login" />" />
	</form>
	<c:if test="${errorLoginPassMessage}">
		<br />
		<div class="alert alert-danger">
			<fmt:message key="message.error" />
		</div>
	</c:if>
	<c:import url="common\line.jsp" />
	<ctg:footer />
</body>
	</html>
</fmt:bundle>