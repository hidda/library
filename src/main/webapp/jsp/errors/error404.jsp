<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:bundle basename="pagetext" prefix="error.">
	<html>
<head>
<title><fmt:message key="label.title404" /></title>
<link href="css/bootstrap.css" rel="stylesheet">
</head>
<body>
	<c:import url="..\common\logo.jsp" />
	<div class="main">
		<fmt:message key="message.request" />
		<br />
		<fmt:message key="message.name" />
		${pageContext.errorData.servletName} <br />
		<fmt:message key="message.code" />
		${pageContext.errorData.statusCode} <br />
		<fmt:message key="message.exception" />
		${pageContext.exception} <br />
		<fmt:message key="message.msg" />
		${pageContext.exception.message}<br /> <a
			href="controller?page=index"><fmt:message key="message.link" /></a>
	</div>
	<c:import url="..\common\line.jsp" />
	<ctg:footer />
</body>
	</html>
</fmt:bundle>