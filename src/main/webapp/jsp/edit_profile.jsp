<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:bundle basename="pagetext" prefix="edit_profile.">
	<html>
<head>
<link href="css/bootstrap.css" rel="stylesheet">
<title><fmt:message key="label.title" /></title>
</head>
<body>
	<c:import url="taghead\header.jsp" />
	<c:import url="common\logo.jsp" />
	<c:import url="tagmenu\head_menu.jsp" />
	<c:if test="${ !checked }">
		<form name="editForm" method="POST" action="controller" class="form">
			<input type="hidden" name="command" value="check_password" /> <input
				type="hidden" name="user_id" value="${ user.id }" />
			<div class="form-group">
				<label for="password"><fmt:message key="message.oldPassword" /></label>
				<input type="password" class="form-control" name="old_password"
					required="required">
			</div>
			<input type="submit" class="btn btn-primary btn-block"
				value="<fmt:message key="button.checkPass" />" />
		</form>
	</c:if>
	<c:if test="${checkError}">
		<div class="alert alert-warning">
			<fmt:message key="message.checkError" />
		</div>
	</c:if>
	<c:if test="${ checked }">
		<form name="editForm" method="POST" action="controller" class="form">
			<input type="hidden" name="command" value="change_password" /> <input
				type="hidden" name="user_id" value="${ user.id }" />
			<div class="form-group">
				<label for="password"><fmt:message key="message.newPassword" /></label>
				<small><fmt:message key="help.password" /></small> <input
					type="password" id="password1" class="form-control" name="password"
					pattern="[\wА-ЯЁа-яё]{8,}"
					title="<fmt:message key="help.password" />" required="required">
			</div>
			<div class="form-group">
				<label for="password"><fmt:message
						key="message.confirmPassword" /></label> <input type="password"
					id="password2" class="form-control" required="required">
			</div>
			<input type="submit" class="btn btn-primary btn-block"
				value="<fmt:message key="button.submit" />" />
		</form>
	</c:if>
	<c:if test="${updateSuccess}">
		<br />
		<c:import url="common\line.jsp" />
		<div class="alert alert-success">
			<fmt:message key="message.updateSuccess" />
		</div>
	</c:if>
	<c:if test="${updateError}">
		<br />
		<c:import url="common\line.jsp" />
		<div class="alert alert-danger">
			<fmt:message key="message.updateError" />
		</div>
	</c:if>
	<c:if test="${validationError}">
		<br />
		<div class="alert alert-danger">
			<fmt:message key="error.validationError" />
		</div>
	</c:if>

	<c:import url="common\line.jsp" />
	<ctg:footer />
</body>
	</html>
</fmt:bundle>