<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:bundle basename="pagetext" prefix="subscriptions.">
	<html>
<head>
<link href="css/bootstrap.css" rel="stylesheet">
<title><fmt:message key="label.title" /></title>
</head>
<body>
	<ctg:roletag role="${ user.role }" />
	<c:import url="taghead\header.jsp" />
	<c:import url="common\logo.jsp" />
	<c:import url="tagmenu\head_menu.jsp" />
	<c:choose>
		<c:when test="${ loadSuccess }">
			<div class="main text-main">
				<c:choose>
					<c:when test="${ admin }">
						<fmt:message key="message.adminHead" />
					</c:when>
					<c:otherwise>
						<fmt:message key="message.readerHead" />
					</c:otherwise>
				</c:choose>
			</div>
			<br />
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<tr>
							<c:if test="${ admin }">
								<th><fmt:message key="table.toClose" /></th>
							</c:if>
							<th><fmt:message key="table.user" /></th>
							<th><fmt:message key="table.userEmail" /></th>
							<th><fmt:message key="table.bookName" /></th>
							<th><fmt:message key="table.bookAuthor" /></th>
							<th><fmt:message key="table.startDate" /></th>
							<th><fmt:message key="table.endDate" /></th>
							<th><fmt:message key="table.overdue" /></th>
							<th><fmt:message key="table.closed" /></th>
						</tr>
					</thead>
					<c:forEach var="sub" items="${subscriptions}" varStatus="status">
						<tbody>
							<c:choose>
								<c:when test="${ sub.overdue }">
									<tr class="danger">
										<c:if test="${ admin }">
											<td align="center" valign="middle"><form
													name="buttomForm" method="POST" action="controller">
													<input type="hidden" name="command"
														value="close_subscription" /> <input type="hidden"
														name="subscription_id" value="${sub.id}"> <input
														type="hidden" name="exemplar_id"
														value="${sub.exemplar.id}"><input type="hidden"
														name="load" value="subscription"><input
														type="image" src="img/icons/close_sub.png"
														title="<fmt:message key="button.title" />" />
												</form></td>
										</c:if>
										<td><a
											href="controller?page=subscriptions&load=user_subscriptions&user_id=${ sub.user.id }"><c:out
													value="${ sub.user.firstName }" /> <c:out
													value="${ sub.user.lastName }" /></a></td>
										<td><c:out value="${ sub.user.email }" /></td>
										<td><c:out value="${ sub.exemplar.book.name}" /></td>
										<td><c:out value="${ sub.exemplar.book.authorFistName }" />
											<c:out value="${ sub.exemplar.book.authorLastName }" /></td>
										<td><c:out value="${ sub.startSubcriptionDate }" /></td>
										<td><c:out value="${ sub.endSubcriptionDate }" /></td>
										<td><fmt:message key="table.isOverdue" /></td>
										<td><fmt:message key="table.nonClosed" /></td>
									</tr>
								</c:when>
								<c:when test="${ sub.closed }">
									<tr class="success">
										<c:if test="${ admin }">
											<td align="center" valign="middle"><fmt:message
													key="table.closedMsg" /></td>
										</c:if>
										<td><a
											href="controller?page=subscriptions&load=user_subscriptions&user_id=${ sub.user.id }"><c:out
													value="${ sub.user.firstName }" /> <c:out
													value="${ sub.user.lastName }" /></a></td>
										<td><c:out value="${ sub.user.email }" /></td>
										<td><c:out value="${ sub.exemplar.book.name}" /></td>
										<td><c:out value="${ sub.exemplar.book.authorFistName }" />
											<c:out value="${ sub.exemplar.book.authorLastName }" /></td>
										<td><c:out value="${ sub.startSubcriptionDate }" /></td>
										<td><c:out value="${ sub.endSubcriptionDate }" /></td>
										<td><fmt:message key="table.nonOverdue" /></td>
										<td><fmt:message key="table.isClosed" /></td>
									</tr>
								</c:when>
								<c:otherwise>
									<tr>
										<c:if test="${ admin }">
											<td align="center" valign="middle"><form
													name="buttomForm" method="POST" action="controller">
													<input type="hidden" name="command"
														value="close_subscription" /> <input type="hidden"
														name="subscription_id" value="${sub.id}"> <input
														type="hidden" name="exemplar_id"
														value="${sub.exemplar.id}"><input type="hidden"
														name="load" value="subscription"><input
														type="image" src="img/icons/close_sub.png"
														title="<fmt:message key="button.title" />" />
												</form></td>
										</c:if>
										<td><a
											href="controller?page=subscriptions&load=user_subscriptions&user_id=${ sub.user.id }"><c:out
													value="${ sub.user.firstName }" /> <c:out
													value="${ sub.user.lastName }" /></a></td>
										<td><c:out value="${ sub.user.email }" /></td>
										<td><c:out value="${ sub.exemplar.book.name}" /></td>
										<td><c:out value="${ sub.exemplar.book.authorFistName }" />
											<c:out value="${ sub.exemplar.book.authorLastName }" /></td>
										<td><c:out value="${ sub.startSubcriptionDate }" /></td>
										<td><c:out value="${ sub.endSubcriptionDate }" /></td>
										<td><fmt:message key="table.nonOverdue" /></td>
										<td><fmt:message key="table.nonClosed" /></td>
									</tr>
								</c:otherwise>
							</c:choose>
						</tbody>
					</c:forEach>
				</table>
			</div>
		</c:when>
		<c:when test="${subscriptionsEmpty}">
			<div class="alert alert-warning">
				<fmt:message key="message.empty" />
			</div>
		</c:when>
		<c:when test="${loadSubscriptionsError}">
			<div class="alert alert-danger">
				<fmt:message key="message.error" />
			</div>
		</c:when>
	</c:choose>
	<c:if test="${ closeSuccess }">
		<br />
		<div class="alert alert-success">
			<fmt:message key="message.closeSuccess" />
		</div>
	</c:if>
	<c:if test="${ errorCloseMessage }">
		<br />
		<div class="alert alert-danger">
			<fmt:message key="message.closeError" />
		</div>
	</c:if>
	<c:import url="common\line.jsp" />
	<ctg:footer />
</body>
	</html>
</fmt:bundle>