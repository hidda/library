<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<html>
<head>
<link href="css/bootstrap.css" rel="stylesheet">
</head>
<body>
	<ctg:roletag role="${ user.role }" />
	<c:choose>
		<c:when test="${guest}">
			<c:import url="taghead\guest_head.jsp" />
		</c:when>
		<c:when test="${admin}">
			<c:import url="taghead\admin_head.jsp" />
		</c:when>
		<c:when test="${reader}">
			<c:import url="taghead\reader_head.jsp" />
		</c:when>
	</c:choose>
</body>
</html>