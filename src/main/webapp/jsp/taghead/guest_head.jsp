<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:bundle basename="pagetext" prefix="header.">
	<html>
<head>
<link href="css/bootstrap.css" rel="stylesheet">
</head>
<body>
	<div class="main" align="right">
		<br />
		<form name="buttomForm" method="POST" action="controller"
			class="button-form">
			<input type="hidden" name="command" value="page" /> <input
				type="hidden" name="page" value="books" /><input type="hidden"
				name="load" value="book" /><input type="hidden" name="load"
				value="genre" /> <input type="image"
				src="<fmt:message key="button.showBooks"/>" />
		</form>
		<form name="buttomForm" method="POST" action="controller"
			class="button-form">
			<input type="hidden" name="command" value="page" /> <input
				type="hidden" name="page" value="registration" /> <input
				type="image" src="<fmt:message key="button.signUp"/>" />
		</form>
		<form name="buttomForm" method="POST" action="controller"
			class="button-form">
			<input type="hidden" name="command" value="page" /> <input
				type="hidden" name="page" value="login" /><input type="image"
				src="<fmt:message key="button.logIn"/>" />
		</form>
		<form name="buttomForm" method="POST" action="controller"
			class="button-form" style="padding-right: 3px">
			<input type="hidden" name="command" value="page" /><input
				type="hidden" name="locale" value="ru_RU" /> <input type="image"
				title="<fmt:message key="button.langru"/>"
				src="img/icons/lang_ru.png" />
		</form>
		<form name="buttomForm" method="POST" action="controller"
			class="button-form" style="padding-right: 3px">
			<input type="hidden" name="command" value="page" /><input
				type="hidden" name="locale" value="en_US" /><input type="image"
				title="<fmt:message key="button.langen"/>"
				src="img/icons/lang_en.png" />
		</form>
	</div>
</body>
	</html>
</fmt:bundle>