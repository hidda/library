<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:bundle basename="pagetext" prefix="header.">
	<html>
<head>
<link href="css/bootstrap.css" rel="stylesheet">
</head>
<body>
	<div class="main" align="right">
		<br />
		<form name="buttomForm" method="POST" action="controller"
			class="button-form">
			<input type="hidden" name="command" value="logout" /><input
				type="image" src="<fmt:message key="button.logout"/>" />
		</form>
	</div>
</body>
	</html>
</fmt:bundle>