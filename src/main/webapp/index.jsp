<meta http-equiv="refresh" content="0; controller?page=main" />

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<fmt:setLocale value="${ locale }" scope="session" />
<fmt:bundle basename="pagetext" prefix="index.">
	<html>
<head>
<link href="css/bootstrap.css" rel="stylesheet">
<title><fmt:message key="label.title" /></title>
</head>
<body>
	<ctg:roletag role="${ user.role }" />
	<c:import url="jsp\common\logo.jsp" />
	<c:import url="jsp\common\line.jsp" />
	<ctg:footer />
</body>
	</html>
</fmt:bundle>