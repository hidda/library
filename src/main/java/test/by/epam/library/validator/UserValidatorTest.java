package test.by.epam.library.validator;

import org.junit.Assert;
import org.junit.Test;

import by.epam.library.entity.User;
import by.epam.library.validator.UserValidator;

public class UserValidatorTest {
	
	@Test
	public void validateTest() {
		User user = new User();
		user.setFirstName("Cecil Gershwin");
		user.setLastName("Palmer");
		user.setLogin("cecil_palmer");
		user.setPassword("allhailthemightyGlowCloud");
		user.setEmail("night_vale_radio@email.email");
		UserValidator userValidator = new UserValidator();
		Assert.assertTrue(userValidator.validate(user));
	}

}
