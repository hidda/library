package test.by.epam.library.pool;

import org.junit.Assert;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;

import by.epam.library.exception.PoolException;
import by.epam.library.pool.ConnectionPool;

public class ConnectionPoolTest {

	@Ignore
	@Test(expected = ExceptionInInitializerError.class)
	public void poolExceptionTest() {
		ConnectionPool pool = ConnectionPool.getInstance();
		try {
			pool.closeAll();
		} catch (PoolException e) {
			Assume.assumeNoException(e);
		}
		Assert.assertNotNull(pool);
	}

	@Test
	public void poolTest() {
		ConnectionPool pool = ConnectionPool.getInstance();
		try {
			pool.closeAll();
		} catch (PoolException e) {
			Assume.assumeNoException(e);
		}
		Assert.assertNotNull(pool);
	}
}
