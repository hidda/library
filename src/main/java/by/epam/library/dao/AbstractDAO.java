package by.epam.library.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.library.entity.Entity;
import by.epam.library.exception.DAOException;
import by.epam.library.exception.PoolException;
import by.epam.library.pool.ConnectionPool;

/**
 * Abstract class that contains all abstract basic methods to work with data
 * base
 * 
 * @author Hidda
 *
 * @param <T extends Entity> - classes, that extends this class, must set any
 *        entity class from this project as a parameter
 */
public abstract class AbstractDAO<T extends Entity> {

	private final static Logger LOG = Logger.getLogger(AbstractDAO.class);
	protected Connection connection;
	protected ConnectionPool pool;

	/**
	 * Creates an object and takes connection with DB from the pool. In case if
	 * pool wasn't initialized before, does it
	 * 
	 * @throws DAOException
	 */
	public AbstractDAO() throws DAOException {
		pool = ConnectionPool.getInstance();
		try {
			this.connection = pool.takeConnection();
		} catch (PoolException e) {
			throw new DAOException("Unable to take connection from pool: " + e);
		}
	}

	/**
	 * Creates an object with the connection from another DAO-object. This was
	 * made to prevent situations in which one command takes several connections
	 * from the pool
	 * 
	 * @param connection
	 */
	public AbstractDAO(Connection connection) {
		this.connection = connection;
	}

	/**
	 * Basic operation for taking all specified record from DB and turn them
	 * into appropriate objects
	 * 
	 * @return List of any specified entity objects from this project
	 */
	public abstract List<T> loadAll();

	/**
	 * Finds record in the DB with specified id and turns it into an object
	 * 
	 * @param id
	 * @return Entity object from this project
	 */
	public abstract T findById(int id);

	/**
	 * Inserts record about new object into the DB
	 * 
	 * @param entity
	 * @return true if operation was successful and false if its failed
	 */
	public abstract boolean create(T entity);

	/**
	 * Deleted specified by id record from the DB
	 * 
	 * @param id
	 * @return true if operation was successful and false if its failed
	 */
	public abstract boolean deleteById(int id);

	/**
	 * Updates specified by entity object record in the DB
	 * 
	 * @param entity
	 * @return true if operation was successful and false if its failed
	 */
	public abstract boolean update(T entity);

	/**
	 * Getter for the connection Purpose of this getter is to provide the
	 * ability to use several DAO-objects with one connection within one command
	 * 
	 * @return
	 */
	public Connection getConnection() {
		return this.connection;
	}

	/**
	 * Returns connection to the pool
	 */
	public void releaseConnection() {
		pool.returnConnection(connection);
	}

	/**
	 * Closes Statement or PrepearedStatement
	 * 
	 * @param statement
	 */
	public void closeStatement(Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				LOG.error("Unable to close statement: " + e);
			}
		}
	}
}
