package by.epam.library.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.library.date.DateManager;
import by.epam.library.entity.Exemplar;
import by.epam.library.entity.Subscription;
import by.epam.library.entity.User;
import by.epam.library.exception.DAOException;
import by.epam.library.manager.SQLRequestManager;

public class SubscriptionDAO extends AbstractDAO<Subscription> {

	private final static Logger LOG = Logger.getLogger(SubscriptionDAO.class);

	private final static String HAS_OVERDUE_KEY = "hasOverdue";
	private final static String HAS_UNCLOSED_KEY = "hasUnclosed";

	private final static String DELETE_BY_ID_REQUEST = "subscription.deleteById";
	private final static String LOAD_ALL_REQUEST = "subscription.loadAll";
	private final static String FIND_BY_ID_REQUEST = "subscription.findById";
	private final static String UPDATE_CLOSED_REQUEST = "subscription.updateClosed";
	private final static String UPDATE_OVERDUE_REQUEST = "subscription.updateOverdue";
	private final static String LOAD_USER_REQUEST = "subscription.loadByUser";
	private final static String LOAD_EXEMPLAR_REQUEST = "subscription.loadByExemplar";
	private final static String CREATE_REQUEST = "subscription.create";

	public SubscriptionDAO() throws DAOException {
	}

	public SubscriptionDAO(Connection connection) {
		super(connection);
	}

	@Override
	public List<Subscription> loadAll() {

		Statement statement = null;
		ResultSet resultSet = null;
		List<Subscription> subscriptionList = null;

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQLRequestManager
					.getRequest(LOAD_ALL_REQUEST));
			UserDAO userDAO = new UserDAO(connection);
			ExemplarDAO exemplarDAO = new ExemplarDAO(connection);
			subscriptionList = new ArrayList<Subscription>();
			while (resultSet.next()) {
				Subscription subscription = new Subscription();
				subscription.setId(resultSet.getInt("subscription_id"));
				subscription.setStartSubcriptionDate(resultSet
						.getDate("sub_start_date"));
				subscription.setEndSubcriptionDate(resultSet
						.getDate("sub_end_date"));
				if ("Y".equals(resultSet.getString("overdue"))) {
					subscription.setOverdue(true);
				} else {
					subscription.setOverdue(false);
				}
				if ("Y".equals(resultSet.getString("closed"))) {
					subscription.setClosed(true);
				} else {
					subscription.setClosed(false);
				}
				User user = userDAO.findById(resultSet.getInt("user_id"));
				subscription.setUser(user);
				Exemplar exemplar = exemplarDAO.findById(resultSet
						.getInt("exemplar_id"));
				subscription.setExemplar(exemplar);
				subscriptionList.add(subscription);
			}
		} catch (SQLException e) {
			LOG.error("Unable to proceed Load All Subscriptions operation: "
					+ e);
		} finally {
			closeStatement(statement);
		}
		return subscriptionList;
	}

	public List<Subscription> loadByUser(User user) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Subscription> subscriptionList = null;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(LOAD_USER_REQUEST));
			preparedStatement.setInt(1, user.getId());
			resultSet = preparedStatement.executeQuery();
			ExemplarDAO exemplarDAO = new ExemplarDAO(connection);
			subscriptionList = new ArrayList<Subscription>();
			while (resultSet.next()) {
				Subscription subscription = new Subscription();
				subscription.setId(resultSet.getInt("subscription_id"));
				subscription.setStartSubcriptionDate(resultSet
						.getDate("sub_start_date"));
				subscription.setEndSubcriptionDate(resultSet
						.getDate("sub_end_date"));
				if ("Y".equals(resultSet.getString("overdue"))) {
					subscription.setOverdue(true);
				} else {
					subscription.setOverdue(false);
				}
				if ("Y".equals(resultSet.getString("closed"))) {
					subscription.setClosed(true);
				} else {
					subscription.setClosed(false);
				}
				subscription.setUser(user);
				Exemplar exemplar = exemplarDAO.findById(resultSet
						.getInt("exemplar_id"));
				subscription.setExemplar(exemplar);
				subscriptionList.add(subscription);
			}

		} catch (SQLException e) {
			LOG.error("Unable to proceed Loas Subscription(s) by User operation: "
					+ e);
		} finally {
			closeStatement(preparedStatement);
		}
		return subscriptionList;
	}

	@Override
	public Subscription findById(int id) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Subscription subscription = null;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(FIND_BY_ID_REQUEST));
			preparedStatement.setInt(1, id);
			resultSet = preparedStatement.executeQuery();
			UserDAO userDAO = new UserDAO(connection);
			ExemplarDAO exemplarDAO = new ExemplarDAO(connection);
			while (resultSet.next()) {
				subscription = new Subscription();
				subscription.setId(id);
				subscription.setStartSubcriptionDate(resultSet
						.getDate("sub_start_date"));
				subscription.setEndSubcriptionDate(resultSet
						.getDate("sub_end_date"));
				if ("Y".equals(resultSet.getString("overdue"))) {
					subscription.setOverdue(true);
				} else {
					subscription.setOverdue(false);
				}
				if ("Y".equals(resultSet.getString("closed"))) {
					subscription.setClosed(true);
				} else {
					subscription.setClosed(false);
				}
				User user = userDAO.findById(resultSet.getInt("user_id"));
				subscription.setUser(user);
				Exemplar exemplar = exemplarDAO.findById(resultSet
						.getInt("exemplar_id"));
				subscription.setExemplar(exemplar);
			}
		} catch (SQLException e) {
			LOG.error("Unable to proceed Find Subscription by Id operation: "
					+ e);
		} finally {
			closeStatement(preparedStatement);
		}
		return subscription;
	}

	@Override
	public boolean create(Subscription subscription) {

		PreparedStatement preparedStatement = null;
		boolean success = false;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(CREATE_REQUEST));
			preparedStatement.setInt(1, subscription.getUser().getId());
			preparedStatement.setInt(2, subscription.getExemplar().getId());
			preparedStatement
					.setDate(3, subscription.getStartSubcriptionDate());
			preparedStatement.setDate(4, subscription.getEndSubcriptionDate());
			preparedStatement.executeUpdate();
			success = true;
		} catch (SQLException e) {
			LOG.error("Unable to proceed Create Subscription operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}

		return success;

	}

	@Override
	public boolean deleteById(int id) {

		PreparedStatement preparedStatement = null;
		boolean success = false;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(DELETE_BY_ID_REQUEST));
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			success = true;
		} catch (SQLException e) {
			LOG.error("Unable to proceed Delete Subscription by Id operation: "
					+ e);
		} finally {
			closeStatement(preparedStatement);
		}
		return success;
	}

	public HashMap<String, Boolean> checkUserSubscriptions(int userId) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		HashMap<String, Boolean> userSubscriptionsStatus = new HashMap<String, Boolean>();
		boolean hasUnclosedSubscriptions = false;
		boolean hasOverdueSubscriptions = false;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(LOAD_USER_REQUEST));
			preparedStatement.setInt(1, userId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				if ("N".equals(resultSet.getString("closed"))) {
					hasUnclosedSubscriptions = true;
				}
				if ("Y".equals(resultSet.getString("overdue"))) {
					hasOverdueSubscriptions = true;
				}
			}
			userSubscriptionsStatus.put(HAS_UNCLOSED_KEY,
					hasUnclosedSubscriptions);
			userSubscriptionsStatus.put(HAS_OVERDUE_KEY,
					hasOverdueSubscriptions);
		} catch (SQLException e) {
			LOG.error("Unable to proceed Check User's Subscription operation: "
					+ e);
		} finally {
			closeStatement(preparedStatement);
		}
		return userSubscriptionsStatus;
	}

	public boolean deleteByUser(int userId) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		boolean success = false;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(LOAD_USER_REQUEST));
			preparedStatement.setInt(1, userId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				preparedStatement = connection
						.prepareStatement(SQLRequestManager
								.getRequest(DELETE_BY_ID_REQUEST));
				preparedStatement
						.setInt(1, resultSet.getInt("subscription_id"));
				preparedStatement.executeUpdate();
			}
			success = true;
		} catch (SQLException e) {
			LOG.error("Unable to proceed Delete Subscription(s) by User operation: "
					+ e);
		} finally {
			closeStatement(preparedStatement);
		}
		return success;
	}

	public boolean deleteByExemplar(int exemplarId) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		boolean success = false;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(LOAD_EXEMPLAR_REQUEST));
			preparedStatement.setInt(1, exemplarId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				preparedStatement = connection
						.prepareStatement(SQLRequestManager
								.getRequest(DELETE_BY_ID_REQUEST));
				preparedStatement
						.setInt(1, resultSet.getInt("subscription_id"));
				preparedStatement.executeUpdate();
			}
			success = true;
		} catch (SQLException e) {
			LOG.error("Unable to proceed Delete Subscription(s) by Exemplar operation: "
					+ e);
		} finally {
			closeStatement(preparedStatement);
		}
		return success;
	}

	@Override
	public boolean update(Subscription entity) {
		throw new UnsupportedOperationException();
	}

	public boolean updateOverdue(List<Subscription> subscriptions) {

		PreparedStatement preparedStatement = null;
		boolean success = false;

		try {
			for (Subscription sub : subscriptions) {
				if (DateManager.checkEndDate(sub.getEndSubcriptionDate())
						&& !sub.isClosed()) {
					preparedStatement = connection
							.prepareStatement(SQLRequestManager
									.getRequest(UPDATE_OVERDUE_REQUEST));
					preparedStatement.setInt(1, sub.getId());
					sub.setOverdue(true);
					preparedStatement.executeUpdate();
				}
			}
			success = true;
		} catch (SQLException e) {
			LOG.error("Unable to proceed Update Subscription (overdue) operation: "
					+ e);
		} finally {
			closeStatement(preparedStatement);
		}
		return success;
	}

	public boolean updateClosed(int id) {

		PreparedStatement preparedStatement = null;
		boolean success = false;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(UPDATE_CLOSED_REQUEST));
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			success = true;
		} catch (SQLException e) {
			LOG.error("Unable to proceed Update Subscription (closed) operation: "
					+ e);
		} finally {
			closeStatement(preparedStatement);
		}
		return success;
	}

}
