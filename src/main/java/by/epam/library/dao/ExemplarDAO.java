package by.epam.library.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.library.entity.Book;
import by.epam.library.entity.Exemplar;
import by.epam.library.entity.ExemplarStatus;
import by.epam.library.exception.DAOException;
import by.epam.library.manager.SQLRequestManager;

public class ExemplarDAO extends AbstractDAO<Exemplar> {

	private final static Logger LOG = Logger.getLogger(ExemplarDAO.class);
	private final static String CREATE_REQUEST = "exemplar.create";
	private final static String FIND_BY_ID_REQUEST = "exemplar.findById";
	private final static String DELETE_BY_ID_REQUEST = "exemplar.deleteById";
	private final static String LOAD_ALL_REQUEST = "exemplar.loadAll";
	private final static String LOAD_BOOK_REQUEST = "exemplar.loadByBook";
	private final static String SET_STATUS_REQUEST = "exemplar.setStatus";

	public ExemplarDAO() throws DAOException {
	}

	public ExemplarDAO(Connection connection) {
		super(connection);
	}

	@Override
	public List<Exemplar> loadAll() {

		Statement statement = null;
		ResultSet resultSet = null;
		List<Exemplar> exemplarList = null;

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQLRequestManager
					.getRequest(LOAD_ALL_REQUEST));
			BookDAO bookDAO = new BookDAO(connection);
			exemplarList = new ArrayList<Exemplar>();
			while (resultSet.next()) {
				Exemplar exemplar = new Exemplar();
				exemplar.setId(resultSet.getInt("exemplar_id"));
				Book book = bookDAO.findById(resultSet.getInt("book_id"));
				exemplar.setBook(book);
				exemplar.setExemplarStatus(resultSet.getString("status"));
				exemplarList.add(exemplar);
			}
		} catch (SQLException e) {
			LOG.error("Unable to proceed Load All Exemplars operation: " + e);
		} finally {
			closeStatement(statement);
		}
		return exemplarList;
	}

	public List<Exemplar> loadByBook(Book book) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Exemplar> exemplarList = null;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(LOAD_BOOK_REQUEST));
			preparedStatement.setInt(1, book.getId());
			exemplarList = new ArrayList<Exemplar>();
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Exemplar exemplar = new Exemplar();
				exemplar.setId(resultSet.getInt("exemplar_id"));
				exemplar.setBook(book);
				exemplar.setExemplarStatus(resultSet.getString("status"));
				exemplarList.add(exemplar);
			}
		} catch (SQLException e) {
			LOG.error("Unable to proceed Loas Exemplars by Book operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}
		return exemplarList;
	}

	@Override
	public Exemplar findById(int id) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Exemplar exemplar = null;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(FIND_BY_ID_REQUEST));
			preparedStatement.setInt(1, id);
			resultSet = preparedStatement.executeQuery();
			BookDAO bookDAO = new BookDAO(connection);

			while (resultSet.next()) {
				exemplar = new Exemplar();
				exemplar.setId(id);
				Book book = bookDAO.findById(resultSet.getInt("book_id"));
				exemplar.setBook(book);
				exemplar.setExemplarStatus(resultSet.getString("status"));
			}
		} catch (SQLException e) {
			LOG.error("Unable to proceed Find Exemplar by Id operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}
		return exemplar;
	}

	@Override
	public boolean create(Exemplar entity) {
		throw new UnsupportedOperationException();
	}

	public boolean create(Book book) {

		PreparedStatement preparedStatement = null;
		boolean success = false;

		try {
			for (int i = 0; i < book.getAmount(); i++) {
				preparedStatement = connection
						.prepareStatement(SQLRequestManager
								.getRequest(CREATE_REQUEST));
				preparedStatement.setInt(1, book.getId());
				preparedStatement.setString(2, ExemplarStatus.FREE.toString()
						.toLowerCase());
				preparedStatement.executeUpdate();
			}
			success = true;
		} catch (SQLException e) {
			LOG.error("Unable to proceed Create Exemplars operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}
		return success;
	}

	@Override
	public boolean deleteById(int id) {

		PreparedStatement preparedStatement = null;
		boolean success = false;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(DELETE_BY_ID_REQUEST));
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			success = true;
		} catch (SQLException e) {
			LOG.error("Unable to proceed Delete Exemplar by Id operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}
		return success;
	}

	public boolean deleteByBook(int bookId) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		boolean success = false;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(LOAD_BOOK_REQUEST));
			preparedStatement.setInt(1, bookId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				preparedStatement = connection
						.prepareStatement(SQLRequestManager
								.getRequest(DELETE_BY_ID_REQUEST));
				preparedStatement.setInt(1, resultSet.getInt("exemplar_id"));
				preparedStatement.executeUpdate();
			}
			success = true;
		} catch (SQLException e) {
			LOG.error("Unable to proceed Delete Exemplar(s) by Book operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}
		return success;
	}

	public boolean updateStatus(int exemplarId, ExemplarStatus exemplarStatus) {

		PreparedStatement preparedStatement = null;
		boolean success = false;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(SET_STATUS_REQUEST));
			preparedStatement.setString(1, exemplarStatus.toString()
					.toLowerCase());
			preparedStatement.setInt(2, exemplarId);
			preparedStatement.executeUpdate();
			success = true;
		} catch (SQLException e) {
			LOG.error("Unable to proceed Update Exemplar Status operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}
		return success;
	}

	@Override
	public boolean update(Exemplar entity) {
		throw new UnsupportedOperationException();
	}
}
