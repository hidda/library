package by.epam.library.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.library.entity.Genre;
import by.epam.library.exception.DAOException;
import by.epam.library.manager.SQLRequestManager;

public class GenreDAO extends AbstractDAO<Genre> {

	private final static Logger LOG = Logger.getLogger(GenreDAO.class);
	private final static String FIND_BY_ID_REQUEST = "genre.findById";
	private final static String LOAD_ALL_REQUEST = "genre.loadAll";
	private final static String CREATE_REQUEST = "genre.create";
	
	public GenreDAO() throws DAOException {
	}
	
	public GenreDAO(Connection connection) {
		super(connection);
	}

	@Override
	public List<Genre> loadAll() {

		Statement statement = null;
		ResultSet resultSet = null;
		List<Genre> genreList = null;

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQLRequestManager
					.getRequest(LOAD_ALL_REQUEST));
			genreList = new ArrayList<Genre>();
			while (resultSet.next()) {
				Genre genre = new Genre();
				genre.setId(resultSet.getInt("genre_id"));
				genre.setGenreType(resultSet.getString("genre"));
				genre.setSubgenre(resultSet.getString("subgenre"));
				genreList.add(genre);
			}
		} catch (SQLException e) {
			LOG.error("Unable to proceed Load All Genres operation: " + e);
		} finally {
			closeStatement(statement);
		}
		return genreList;
	}

	@Override
	public Genre findById(int id) {
		
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Genre genre = null;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager.getRequest(FIND_BY_ID_REQUEST));
			preparedStatement.setInt(1, id);
			resultSet = preparedStatement.executeQuery();
			
			if (resultSet.next()) {
				genre = new Genre();
				genre.setId(id);
				genre.setGenreType(resultSet.getString("genre"));
				genre.setSubgenre(resultSet.getString("subgenre"));
			}
		} catch (SQLException e) {
			LOG.error("Unable to proceed Find Genre by Id operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}
		return genre;
	}

	@Override
	public boolean create(Genre genre) {
		
		PreparedStatement preparedStatement = null;
		boolean success = false;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager.getRequest(CREATE_REQUEST));
			preparedStatement.setString(1, genre.getGenreType().toString().toLowerCase());
			preparedStatement.setString(2, genre.getSubgenre());
			preparedStatement.executeUpdate();
			success = true;
		} catch (SQLException e) {
			LOG.error("Unable to proceed Create Genre operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}
		return success;

	}

	@Override
	public boolean deleteById(int id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean update(Genre entity) {
		throw new UnsupportedOperationException();
	}

}
