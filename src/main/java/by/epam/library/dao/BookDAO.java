package by.epam.library.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.library.entity.Book;
import by.epam.library.entity.Genre;
import by.epam.library.exception.DAOException;
import by.epam.library.manager.SQLRequestManager;

public class BookDAO extends AbstractDAO<Book> {

	private final static Logger LOG = Logger.getLogger(BookDAO.class);
	
	private final static String CREATE_REQUEST = "book.create";
	private final static String FIND_BY_ID_REQUEST = "book.findById";
	private final static String DELETE_BY_ID_REQUEST = "book.deleteById";
	private final static String LOAD_ALL_REQUEST = "book.loadAll";
	private final static String LOAD_GENRE_REQUEST = "book.loadByGenre";
	private final static String DECREASE_NUMBER_REQUEST = "book.decreaseNumber";

	public BookDAO() throws DAOException {
	}
	
	public BookDAO(Connection connection) {
		super(connection);
	}

	@Override
	public List<Book> loadAll() {

		Statement statement = null;
		ResultSet resultSet = null;
		List<Book> bookList = null;

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQLRequestManager
					.getRequest(LOAD_ALL_REQUEST));
			bookList = new ArrayList<Book>();
			while (resultSet.next()) {
				Book book = new Book();
				book.setId(resultSet.getInt("book_id"));
				book.setName(resultSet.getString("name"));
				book.setAuthorFistName(resultSet.getString("author_first_name"));
				book.setAuthorLastName(resultSet.getString("author_last_name"));
				book.setPublisher(resultSet.getString("publisher"));
				book.setYear(resultSet.getInt("year"));
				book.setAmount(resultSet.getInt("amount"));
				Genre genre = new Genre();
				genre.setId(resultSet.getInt("genre_id"));
				genre.setGenreType(resultSet.getString("genre"));
				genre.setSubgenre(resultSet.getString("subgenre"));
				book.setGenre(genre);
				bookList.add(book);
			}
		} catch (SQLException e) {
			LOG.error("Unable to proceed Load All Books operation: " + e);
		} finally {
			closeStatement(statement);
		}
		return bookList;
	}
	
	public List<Book> loadByGenre(Genre genre) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Book> bookList = null;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(LOAD_GENRE_REQUEST));
			preparedStatement.setInt(1, genre.getId());
			resultSet = preparedStatement.executeQuery();
			bookList = new ArrayList<Book>();
			while (resultSet.next()) {
				Book book = new Book();
				book.setId(resultSet.getInt("book_id"));
				book.setName(resultSet.getString("name"));
				book.setAuthorFistName(resultSet.getString("author_first_name"));
				book.setAuthorLastName(resultSet.getString("author_last_name"));
				book.setPublisher(resultSet.getString("publisher"));
				book.setYear(resultSet.getInt("year"));
				book.setAmount(resultSet.getInt("amount"));
				book.setGenre(genre);
				bookList.add(book);
			}
		} catch (SQLException e) {
			LOG.error("Unable to proceed Load Books by Genre operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}
		return bookList;
	}


	@Override
	public Book findById(int id) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Book book = null;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(FIND_BY_ID_REQUEST));
			preparedStatement.setInt(1, id);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				book = new Book();
				Genre genre = new Genre();
				genre.setId(resultSet.getInt("genre_id"));
				genre.setGenreType(resultSet.getString("genre"));
				genre.setSubgenre(resultSet.getString("subgenre"));
				book.setId(resultSet.getInt("book_id"));
				book.setName(resultSet.getString("name"));
				book.setAuthorFistName(resultSet.getString("author_first_name"));
				book.setAuthorLastName(resultSet.getString("author_last_name"));
				book.setPublisher(resultSet.getString("publisher"));
				book.setYear(resultSet.getInt("year"));
				book.setAmount(resultSet.getInt("amount"));
				book.setGenre(genre);
			}
		} catch (SQLException e) {
			LOG.error("Unable to proceed Find Book by Id operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}
		return book;
	}

	@Override
	public boolean create(Book book) {

		PreparedStatement preparedStatement = null;
		boolean success = false;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(CREATE_REQUEST), PreparedStatement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, book.getName());
			preparedStatement.setString(2, book.getAuthorFistName());
			preparedStatement.setString(3, book.getAuthorLastName());
			preparedStatement.setString(4, book.getPublisher());
			preparedStatement.setInt(5, book.getYear());
			preparedStatement.setInt(6, book.getAmount());
			preparedStatement.setInt(7, book.getGenre().getId());
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			if (resultSet.next()) {
				book.setId(resultSet.getInt(1));
			}
			success = true;
		} catch (SQLException e) {
			LOG.error("Unable to proceed Create Book operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}
		return success;

	}

	@Override
	public boolean deleteById(int id) {

		PreparedStatement preparedStatement = null;
		boolean success = false;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager.getRequest(DELETE_BY_ID_REQUEST));
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			success = true;
		} catch (SQLException e) {
			LOG.error("Unable to proceed Delete Book by Id operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}
		return success;
	}
	
	public boolean decreaseNumber(Book book) {
		
		PreparedStatement preparedStatement = null;
		boolean success = false;
		
		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager.getRequest(DECREASE_NUMBER_REQUEST));
			int decreasedAmount = book.getAmount() - 1;
			preparedStatement.setInt(1, decreasedAmount);
			preparedStatement.setInt(2, book.getId());
			preparedStatement.executeUpdate();
			success = true;
		} catch (SQLException e) {
			LOG.error("Unable to proceed Decrease Amount of Book operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}
		return success;
	}

	@Override
	public boolean update(Book entity) {
		throw new UnsupportedOperationException();
	}
}
