package by.epam.library.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.library.entity.User;
import by.epam.library.exception.DAOException;
import by.epam.library.manager.SQLRequestManager;

public class UserDAO extends AbstractDAO<User> {

	private final static Logger LOG = Logger.getLogger(UserDAO.class);
	private final static String LOGIN_REQUEST = "user.login";
	private final static String CREATE_REQUEST = "user.create";
	private final static String FIND_BY_ID_REQUEST = "user.findById";
	private final static String DELETE_BY_ID_REQUEST = "user.deleteById";
	private final static String LOAD_LOGIN_REQUEST = "user.loadLogins";
	private final static String LOAD_INFO_REQUEST = "user.loadInfo";
	private final static String CHECK_PASSWORD_REQUEST = "user.checkPassword";
	private final static String UPDATE_REQUEST = "user.update";

	public UserDAO() throws DAOException {
	}

	public UserDAO(Connection connection) {
		super(connection);
	}

	public User findByLoginPassword(String login, String password)
			throws DAOException {

		User user = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(LOGIN_REQUEST));
			preparedStatement.setString(1, login);
			preparedStatement.setString(2, password);

			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				user = new User();
				user.setId(resultSet.getInt("user_id"));
				user.setLogin(resultSet.getString("login"));
				user.setFirstName(resultSet.getString("first_name"));
				user.setLastName(resultSet.getString("last_name"));
				user.setEmail(resultSet.getString("email"));
				user.setRole(resultSet.getString("role"));
			}
		} catch (SQLException e) {
			LOG.error("Unable to proceed Login operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}
		return user;
	}

	@Override
	public List<User> loadAll() {
		throw new UnsupportedOperationException();
	}

	public List<User> loadUserInfo() {
		Statement statement = null;
		ResultSet resultSet = null;
		List<User> userInfoList = null;

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQLRequestManager
					.getRequest(LOAD_INFO_REQUEST));
			userInfoList = new ArrayList<User>();
			while (resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt("user_id"));
				user.setFirstName(resultSet.getString("first_name"));
				user.setLastName(resultSet.getString("last_name"));
				user.setEmail(resultSet.getString("email"));
				userInfoList.add(user);
			}
		} catch (SQLException e) {
			LOG.error("Unable to proceed Load User Info operation: " + e);
		} finally {
			closeStatement(statement);
		}
		return userInfoList;
	}

	@Override
	public User findById(int id) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		User user = null;

		try {

			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(FIND_BY_ID_REQUEST));
			preparedStatement.setInt(1, id);
			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				user = new User();
				user.setId(id);
				user.setLogin(resultSet.getString("login"));
				user.setFirstName(resultSet.getString("first_name"));
				user.setLastName(resultSet.getString("last_name"));
				user.setEmail(resultSet.getString("email"));
				user.setRole(resultSet.getString("role"));
			}
		} catch (SQLException e) {
			LOG.error("Unable to proceed Find User by Id operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}
		return user;
	}

	@Override
	public boolean create(User user) {

		PreparedStatement preparedStatement = null;
		boolean success = false;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(CREATE_REQUEST));
			preparedStatement.setString(1, user.getLogin());
			preparedStatement.setString(2, user.getPassword());
			preparedStatement.setString(3, user.getFirstName());
			preparedStatement.setString(4, user.getLastName());
			preparedStatement.setString(5, user.getEmail());
			preparedStatement.setString(6, user.getRole().toString()
					.toLowerCase());
			preparedStatement.executeUpdate();
			success = true;
		} catch (SQLException e) {
			LOG.error("Unable to proceed Create User operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}
		return success;
	}

	@Override
	public boolean deleteById(int id) {

		PreparedStatement preparedStatement = null;
		boolean success = false;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(DELETE_BY_ID_REQUEST));
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			success = true;
		} catch (SQLException e) {
			LOG.error("Unable to proceed Delete User by Id operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}
		return success;
	}

	public boolean changePassword(int userId, String password) {
		PreparedStatement preparedStatement = null;
		boolean success = false;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(UPDATE_REQUEST));
			preparedStatement.setString(1, password);
			preparedStatement.setInt(2, userId);
			preparedStatement.executeUpdate();
			success = true;
		} catch (SQLException e) {
			LOG.error("Unable to proceed Change Password operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}
		return success;

	}

	public boolean checkPassword(String password, int userId) {

		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		boolean success = false;

		try {
			preparedStatement = connection.prepareStatement(SQLRequestManager
					.getRequest(CHECK_PASSWORD_REQUEST));
			preparedStatement.setString(1, password);
			preparedStatement.setInt(2, userId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				success = true;
			}
		} catch (SQLException e) {
			LOG.error("Unable to proceed Check Password operation: " + e);
		} finally {
			closeStatement(preparedStatement);
		}
		return success;
	}

	public List<String> loadLogins() {

		Statement statement = null;
		ResultSet resultSet = null;
		List<String> logins = new ArrayList<String>();

		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(SQLRequestManager
					.getRequest(LOAD_LOGIN_REQUEST));
			while (resultSet.next()) {
				logins.add(resultSet.getString("login"));
			}
		} catch (SQLException e) {
			LOG.error("Unable to proceed Load Logins operation: " + e);
		} finally {
			closeStatement(statement);
		}

		return logins;
	}

	@Override
	public boolean update(User entity) {
		throw new UnsupportedOperationException();
	}

}
