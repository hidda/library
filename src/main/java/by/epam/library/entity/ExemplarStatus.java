package by.epam.library.entity;

public enum ExemplarStatus {

	FREE, LIBRARY_ROOM, TAKEN
	
}
