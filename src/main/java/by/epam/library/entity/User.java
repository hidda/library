package by.epam.library.entity;

public class User extends Entity {

	private String login;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private UserRole role;

	public User() {
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = UserRole.valueOf(role.toUpperCase());
	}

	@Override
	public int hashCode() {
		int code = 42 * this.getId()
				+ ((this.email != null) ? this.email.hashCode() : 42);
		return code;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		User other = (User) obj;
		if (!(this.getId() == other.getId())) {
			return false;
		}
		if (!this.login.equals(other.getLogin())) {
			return false;
		}
		if (!this.email.equals(other.getEmail())) {
			return false;
		}
		return true;
	}
}
