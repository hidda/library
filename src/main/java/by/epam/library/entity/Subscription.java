package by.epam.library.entity;

import java.sql.Date;

public class Subscription extends Entity {

	private User user;
	private Exemplar exemplar;
	private Date startSubcriptionDate;
	private Date endSubcriptionDate;
	private boolean isOverdue;
	private boolean isClosed;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Exemplar getExemplar() {
		return exemplar;
	}

	public void setExemplar(Exemplar exemplar) {
		this.exemplar = exemplar;
	}

	public Date getStartSubcriptionDate() {
		return startSubcriptionDate;
	}

	public void setStartSubcriptionDate(Date startSubcriptionDate) {
		this.startSubcriptionDate = startSubcriptionDate;
	}

	public Date getEndSubcriptionDate() {
		return endSubcriptionDate;
	}

	public void setEndSubcriptionDate(Date endSubcriptionDate) {
		this.endSubcriptionDate = endSubcriptionDate;
	}

	public boolean isOverdue() {
		return isOverdue;
	}

	public void setOverdue(boolean isOverdue) {
		this.isOverdue = isOverdue;
	}

	public boolean isClosed() {
		return isClosed;
	}

	public void setClosed(boolean isClosed) {
		this.isClosed = isClosed;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Subscription other = (Subscription) obj;
		if (!(this.getId() == other.getId())) {
			return false;
		}
		if (!this.exemplar.equals(other.getExemplar())) {
			return false;
		}
		if (!this.user.equals(other.getUser())) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		int code = 42 * this.getId()
				+ ((this.user != null) ? this.user.hashCode() : 42)
				+ ((this.exemplar != null) ? this.exemplar.hashCode() : 23);
		return code;
	}
}
