package by.epam.library.entity;

public class Book extends Entity {

	private String name;
	private String authorFistName;
	private String authorLastName;
	private String publisher;
	private int year;
	private int amount;
	private Genre genre;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthorFistName() {
		return authorFistName;
	}

	public void setAuthorFistName(String authorFistName) {
		this.authorFistName = authorFistName;
	}

	public String getAuthorLastName() {
		return authorLastName;
	}

	public void setAuthorLastName(String authorLastName) {
		this.authorLastName = authorLastName;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	@Override
	public int hashCode() {
		int code = 42
				* this.getId()
				+ ((this.name != null) ? this.name.hashCode() : 42)
				+ ((this.authorLastName != null) ? this.authorLastName
						.hashCode() : 23);
		return code;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Book other = (Book) obj;
		if (!(this.getId() == other.getId())) {
			return false;
		}
		if (!this.name.equals(other.getName())) {
			return false;
		}
		if (!this.authorLastName.equals(other.getAuthorLastName())) {
			return false;
		}
		if (!this.publisher.equals(other.getPublisher())) {
			return false;
		}
		return true;
	}

}
