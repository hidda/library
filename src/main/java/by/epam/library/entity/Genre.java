package by.epam.library.entity;

public class Genre extends Entity {

	private GenreType genreType;
	private String subgenre;

	public GenreType getGenreType() {
		return genreType;
	}

	public void setGenreType(String genreType) {
		this.genreType = GenreType.valueOf(genreType.toUpperCase());
	}

	public String getSubgenre() {
		return subgenre;
	}

	public void setSubgenre(String subgenre) {
		this.subgenre = subgenre;
	}

	@Override
	public int hashCode() {
		int code = 42
				* this.getId()
				+ ((this.subgenre != null) ? this.subgenre.hashCode() : 42)
				+ ((this.genreType.toString() != null) ? this.genreType
						.toString().hashCode() : 23);
		return code;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Genre other = (Genre) obj;
		if (!(this.getId() == other.getId())) {
			return false;
		}
		if (!this.subgenre.equals((other.getSubgenre()))) {
			return false;
		}
		return true;
	}

}
