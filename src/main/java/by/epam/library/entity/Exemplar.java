package by.epam.library.entity;

public class Exemplar extends Entity {

	private Book book;
	private ExemplarStatus exemplarStatus;

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public ExemplarStatus getExemplarStatus() {
		return exemplarStatus;
	}

	public void setExemplarStatus(String exemplarStatus) {
		this.exemplarStatus = ExemplarStatus.valueOf(exemplarStatus
				.toUpperCase());
	}

	@Override
	public int hashCode() {
		int code = 42
				* this.getId()
				+ ((this.book.getName() != null) ? this.book.getName()
						.hashCode() : 42)
				+ ((this.exemplarStatus.toString() != null) ? this.exemplarStatus
						.toString().hashCode() : 23);
		return code;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Exemplar other = (Exemplar) obj;
		if (!(this.getId() == other.getId())) {
			return false;
		}
		if (!this.book.equals(other.getBook())) {
			return false;
		}
		if (!this.exemplarStatus.equals(other.getExemplarStatus())) {
			return false;
		}
		return true;
	}
}
