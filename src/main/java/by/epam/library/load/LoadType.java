package by.epam.library.load;

public enum LoadType {
	
	BOOK, EXEMPLAR, SUBSCRIPTION, GENRE, SUB_INFO, BOOK_EXEMPLARS, USER_SUBSCRIPTIONS, GENRE_BOOKS;

}
