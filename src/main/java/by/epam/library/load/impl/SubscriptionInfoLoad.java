package by.epam.library.load.impl;

import java.util.HashMap;
import java.util.List;

import by.epam.library.command.ContentManager;
import by.epam.library.entity.Exemplar;
import by.epam.library.entity.User;
import by.epam.library.load.ILoad;
import by.epam.library.logic.LoadByParameterLogic;
import by.epam.library.logic.LoadLogic;

public class SubscriptionInfoLoad implements ILoad {

	private final static String ID_PARAMETER = "exemplar_id";

	private final static String ERROR_KEY = "loadError";

	@Override
	public void load(ContentManager contentManager) {

		HashMap<String, String> parameterMap = contentManager
				.getRequestParameters();

		HashMap<String, Object> requestAttributes = new HashMap<String, Object>();
		List<User> usersInfo = LoadLogic.loadUserInfo();
		Exemplar exemplar = new Exemplar();
		int id = Integer.parseInt(parameterMap.get(ID_PARAMETER));
		exemplar = LoadByParameterLogic.loadExemplar(id);
		if (usersInfo != null) {
			requestAttributes.put("userInfo", usersInfo);
			requestAttributes.put("exemplar", exemplar);
		} else {
			requestAttributes.put(ERROR_KEY, true);
		}
		contentManager.setRequestAttributes(requestAttributes);
	}
}
