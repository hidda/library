package by.epam.library.load.impl;

import java.util.HashMap;
import java.util.List;

import by.epam.library.command.ContentManager;
import by.epam.library.entity.Genre;
import by.epam.library.load.ILoad;
import by.epam.library.logic.LoadLogic;

public class GenresLoad implements ILoad {

	private final static String ERROR_KEY = "loadGenresError";
	
	private final static String GENRES_LIST_KEY = "genres";

	@Override
	public void load(ContentManager contentManager) {

		List<Genre> genres = LoadLogic.loadAllGenres();
		HashMap<String, Object> requestAttributes = new HashMap<String, Object>();

		if (genres != null && !genres.isEmpty()) {
			requestAttributes.put(GENRES_LIST_KEY, genres);
		} else {
			requestAttributes.put(ERROR_KEY, true);
		}
		contentManager.setRequestAttributes(requestAttributes);
	}
}
