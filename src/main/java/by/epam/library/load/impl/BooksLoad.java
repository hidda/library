package by.epam.library.load.impl;

import java.util.HashMap;
import java.util.List;

import by.epam.library.command.ContentManager;
import by.epam.library.entity.Book;
import by.epam.library.load.ILoad;
import by.epam.library.logic.LoadLogic;

public class BooksLoad implements ILoad {

	private final static String BOOKS_LIST_KEY = "books";
	
	private final static String SUCCESS_KEY = "loadSuccess";
	private final static String ERROR_KEY = "loadBooksError";
	private final static String EMPTY_KEY = "booksEmpty";

	@Override
	public void load(ContentManager contentManager) {

		List<Book> books = LoadLogic.loadAllBooks();
		HashMap<String, Object> requestAttributes = new HashMap<String, Object>();

		if (books != null && !books.isEmpty()) {
			requestAttributes.put(BOOKS_LIST_KEY, books);
			requestAttributes.put(SUCCESS_KEY, true);
		} else if (books == null) {
			requestAttributes.put(ERROR_KEY, true);
		} else {
			requestAttributes.put(EMPTY_KEY, true);
		}
		contentManager.setRequestAttributes(requestAttributes);
	}

}
