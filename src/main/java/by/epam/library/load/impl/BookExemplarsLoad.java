package by.epam.library.load.impl;

import java.util.HashMap;
import java.util.List;

import by.epam.library.command.ContentManager;
import by.epam.library.entity.Exemplar;
import by.epam.library.load.ILoad;
import by.epam.library.logic.LoadByParameterLogic;

public class BookExemplarsLoad implements ILoad {

	private final static String ID_PARAMETER = "book_id";
	
	private final static String EXEMPLARS_LIST_KEY = "exemplars";
	
	private final static String SUCCESS_KEY = "loadSuccess";
	private final static String ERROR_KEY = "loadExemplarsError";
	private final static String EMPTY_KEY = "exemplarsEmpty";

	@Override
	public void load(ContentManager contentManager) {
		HashMap<String, String> parameterMap = contentManager
				.getRequestParameters();

		int bookId = Integer.parseInt(parameterMap.get(ID_PARAMETER));
		List<Exemplar> exemplars = LoadByParameterLogic
				.loadBookExemplars(bookId);
		HashMap<String, Object> requestAttributes = new HashMap<String, Object>();

		if (exemplars != null && !exemplars.isEmpty()) {
			requestAttributes.put(EXEMPLARS_LIST_KEY, exemplars);
			requestAttributes.put(SUCCESS_KEY, true);
		} else if (exemplars == null) {
			requestAttributes.put(ERROR_KEY, true);
		} else {
			requestAttributes.put(EMPTY_KEY, true);
		}
		contentManager.setRequestAttributes(requestAttributes);
	}
}
