package by.epam.library.load.impl;

import java.util.HashMap;
import java.util.List;

import by.epam.library.command.ContentManager;
import by.epam.library.entity.Subscription;
import by.epam.library.load.ILoad;
import by.epam.library.logic.LoadByParameterLogic;

public class UserSubscriptionsLoad implements ILoad {

	private final static String ID_PARAMETER = "user_id";
	
	private final static String SUBSCRIPTIONS_LIST_KEY = "subscriptions";
	
	private final static String SUCCESS_KEY = "loadSuccess";
	private final static String ERROR_KEY = "loadSubscriptionsError";
	private final static String EMPTY_KEY = "subscriptionsEmpty";

	@Override
	public void load(ContentManager contentManager) {
		HashMap<String, String> parameterMap = contentManager
				.getRequestParameters();

		int userId = Integer.parseInt(parameterMap.get(ID_PARAMETER));
		List<Subscription> subscriptions = LoadByParameterLogic
				.loadSubscriptions(userId);
		HashMap<String, Object> requestAttributes = new HashMap<String, Object>();

		if (subscriptions != null && !subscriptions.isEmpty()) {
			requestAttributes.put(SUBSCRIPTIONS_LIST_KEY, subscriptions);
			requestAttributes.put(SUCCESS_KEY, true);
		} else if (subscriptions == null) {
			requestAttributes.put(ERROR_KEY, true);
		} else {
			requestAttributes.put(EMPTY_KEY, true);
		}
		contentManager.setRequestAttributes(requestAttributes);
	}
}
