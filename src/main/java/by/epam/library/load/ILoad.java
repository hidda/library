package by.epam.library.load;

import by.epam.library.command.ContentManager;

/**
 * Basic interface to implement load operations
 * 
 * @author Hidda
 *
 */
public interface ILoad {
	/**
	 * Implementations must do specified load operations. Load interface differs
	 * from the command interface that it doesn't return target pages. This
	 * interface was made to separate commands from loading all additional
	 * information, that is needed to execute commands.
	 * 
	 * @param contentManager
	 *            - object than handles request and session content
	 */
	public void load(ContentManager contentManager);

}
