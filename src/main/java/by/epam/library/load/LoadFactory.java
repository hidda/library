package by.epam.library.load;

import by.epam.library.load.impl.BookExemplarsLoad;
import by.epam.library.load.impl.BooksLoad;
import by.epam.library.load.impl.ExemplarLoad;
import by.epam.library.load.impl.GenreBooksLoad;
import by.epam.library.load.impl.GenresLoad;
import by.epam.library.load.impl.SubscriptionLoad;
import by.epam.library.load.impl.SubscriptionInfoLoad;
import by.epam.library.load.impl.UserSubscriptionsLoad;

public class LoadFactory {

	public static ILoad createLoad(String load) {
		LoadType loadType = LoadType.valueOf(load.toUpperCase());
		switch (loadType) {
		case GENRE:
			return new GenresLoad();
		case EXEMPLAR:
			return new ExemplarLoad();
		case SUBSCRIPTION:
			return new SubscriptionLoad();
		case SUB_INFO:
			return new SubscriptionInfoLoad();
		case BOOK_EXEMPLARS:
			return new BookExemplarsLoad();
		case GENRE_BOOKS:
			return new GenreBooksLoad();
		case USER_SUBSCRIPTIONS:
			return new UserSubscriptionsLoad();
		case BOOK:
			return new BooksLoad();
		}
		return null;
	}

}
