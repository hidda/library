package by.epam.library.date;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.log4j.Logger;

public class DateManager {

	private final static Logger LOG = Logger.getLogger(DateManager.class);
	private static SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd");

	public static boolean checkEndDate(Date endDate) {
		boolean isOverdue = false;
		Calendar cal = Calendar.getInstance();
		if (endDate.before(cal.getTime())) {
			isOverdue = true;
		}
		return isOverdue;
	}
	
	public static Date getDate(String stringDate) {
		
		Date date = null;

		try {
			stringDate = dateFormat.format(dateFormat.parse(stringDate));
			date = Date.valueOf(stringDate);
		} catch (ParseException e) {
			LOG.error("Data parsing error");
		}
		return date;
	}

	public static Date getStartDate() {
		Calendar cal = Calendar.getInstance();
		Date today = Date.valueOf(dateFormat.format(cal.getTime()));
		return today;
	}

	public static Date autoEndSubDate() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, 1);
		String autoDate = dateFormat.format(cal.getTime());
		Date date = Date.valueOf(autoDate);
		return date;
	}
}
