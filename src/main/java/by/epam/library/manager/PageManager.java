package by.epam.library.manager;

import java.util.ResourceBundle;

public class PageManager {

	private final static String BUNDLE_NAME = "pagename";
	private final static ResourceBundle BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	public static String getPageName(String key) {
		String pagePath = BUNDLE.getString(key);
		return pagePath;
	}
}
