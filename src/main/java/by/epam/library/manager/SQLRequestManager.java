package by.epam.library.manager;

import java.util.ResourceBundle;

public class SQLRequestManager {
	
	private final static String BUNDLE_NAME = "sqlrequest";
	private final static ResourceBundle BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	public static String getRequest(String key) {
		String request = BUNDLE.getString(key);
		return request;
	}

}
