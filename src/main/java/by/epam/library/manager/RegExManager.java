package by.epam.library.manager;

import java.util.ResourceBundle;

public class RegExManager {

	private final static String BUNDLE_NAME = "regex";
	private final static ResourceBundle BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	public static String getPageName(String key) {
		String pattern = BUNDLE.getString(key);
		return pattern;
	}
}
