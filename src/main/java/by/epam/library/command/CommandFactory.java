package by.epam.library.command;

import java.util.Optional;

import by.epam.library.command.impl.CheckPasswordCommand;
import by.epam.library.command.impl.CloseSubscriptionCommand;
import by.epam.library.command.impl.CreateBookCommand;
import by.epam.library.command.impl.CreateSubscriptionCommand;
import by.epam.library.command.impl.CreateUserCommand;
import by.epam.library.command.impl.DeleteBookCommand;
import by.epam.library.command.impl.DeleteExemplarCommand;
import by.epam.library.command.impl.DeleteSubscriptionCommand;
import by.epam.library.command.impl.DeleteUserCommand;
import by.epam.library.command.impl.LoginCommand;
import by.epam.library.command.impl.LogoutCommand;
import by.epam.library.command.impl.PageCommand;
import by.epam.library.command.impl.ChangePasswordCommand;

public class CommandFactory {
	
	private final static String DEFAULT_COMMAND = "page";

	public static ICommand createCommand(String command) {
		Optional<String> opt = Optional.ofNullable(command);
		CommandType com = CommandType.valueOf(opt.orElse(DEFAULT_COMMAND).toUpperCase());
		switch (com) {
		case LOGIN:
			return new LoginCommand();
		case LOGOUT:
			return new LogoutCommand();
		case CREATE_USER:
			return new CreateUserCommand();
		case CREATE_BOOK:
			return new CreateBookCommand();
		case CREATE_SUBSCRIPTION:
			return new CreateSubscriptionCommand();
		case CLOSE_SUBSCRIPTION:
			return new CloseSubscriptionCommand();
		case DELETE_BOOK:
			return new DeleteBookCommand();
		case DELETE_EXEMPLAR:
			return new DeleteExemplarCommand();
		case DELETE_SUBSCRIPTION:
			return new DeleteSubscriptionCommand();
		case DELETE_USER:
			return new DeleteUserCommand();
		case CHECK_PASSWORD:
			return new CheckPasswordCommand();
		case CHANGE_PASSWORD:
			return new ChangePasswordCommand();
		default:
			return new PageCommand();
		}
	}
}
