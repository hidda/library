package by.epam.library.command;

/**
 * Basic interface for executing commands.
 *
 */
public interface ICommand {
	/**
	 * Implementations must do specified commands and define target pages for
	 * success and failed statuses of executed operations
	 * 
	 * @param contentManager
	 *            - object than handles request and session content
	 * @return target page address
	 */
	String execute(ContentManager contentManager);

}
