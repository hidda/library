package by.epam.library.command.impl;

import java.util.HashMap;
import java.util.Optional;

import by.epam.library.command.ContentManager;
import by.epam.library.command.ICommand;
import by.epam.library.manager.PageManager;

public class PageCommand implements ICommand {

	private final static String PAGE_NAME = "page";
	private final static String DEFAULT_PAGE_NAME = "main";

	private final static String LOCALE = "locale";

	@Override
	public String execute(ContentManager contentManager) {
		HashMap<String, String> parameterMap = contentManager
				.getRequestParameters();

		Optional<String> opt = Optional.ofNullable(parameterMap.get(PAGE_NAME));
		String page = PageManager.getPageName(opt.orElse(DEFAULT_PAGE_NAME));
		if (parameterMap.get(LOCALE) != null) {
			HashMap<String, Object> sessionAttributes = new HashMap<String, Object>();
			sessionAttributes.put(LOCALE, parameterMap.get(LOCALE));
			contentManager.setSessionAttrubutes(sessionAttributes);
		}
		return page;
	}
}
