package by.epam.library.command.impl;

import java.util.HashMap;

import by.epam.library.command.ContentManager;
import by.epam.library.command.ICommand;
import by.epam.library.logic.UpdateLogic;
import by.epam.library.manager.PageManager;

public class CloseSubscriptionCommand implements ICommand {

	private final static String SUBSCRIPTION_ID_PARAMETER = "subscription_id";
	private final static String EXEMPLAR_ID_PARAMETER = "exemplar_id";

	private final static String PAGE_NAME = "subscriptions";
	private final static String SUCCESS_KEY = "closeSuccess";
	private final static String ERROR_KEY = "errorCloseMessage";

	@Override
	public String execute(ContentManager contentManager) {

		HashMap<String, String> parameterMap = contentManager
				.getRequestParameters();
		HashMap<String, Object> requestAttributes = new HashMap<String, Object>();
		int subscriptionId = Integer.parseInt(parameterMap
				.get(SUBSCRIPTION_ID_PARAMETER));
		int exemplarId = Integer.parseInt(parameterMap
				.get(EXEMPLAR_ID_PARAMETER));

		if (UpdateLogic.closeSubscription(subscriptionId, exemplarId)) {
			requestAttributes.put(SUCCESS_KEY, true);
		} else {
			requestAttributes.put(ERROR_KEY, true);
		}
		String page = PageManager.getPageName(PAGE_NAME);
		requestAttributes.put(EXEMPLAR_ID_PARAMETER, exemplarId);
		contentManager.setRequestAttributes(requestAttributes);
		return page;
	}
}
