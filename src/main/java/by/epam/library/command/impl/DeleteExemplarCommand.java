package by.epam.library.command.impl;

import java.util.HashMap;

import by.epam.library.command.ContentManager;
import by.epam.library.command.ICommand;
import by.epam.library.entity.ExemplarStatus;
import by.epam.library.logic.DeleteLogic;
import by.epam.library.manager.PageManager;

public class DeleteExemplarCommand implements ICommand {

	private final static String ID_PARAMETER = "exemplar_id";
	private final static String BOOK_ID_PARAMETER = "book_id";
	private final static String EXEMPLAR_STATUS_PARAMETER = "status";

	private final static String PAGE_NAME = "exemplars";
	private final static String SUCCESS_KEY = "deleteSuccess";
	private final static String ERROR_KEY = "errorDeleteMessage";
	private final static String HAS_SUBSCRIPTIONS_ERROR_KEY = "hasSubscriptionsError";

	@Override
	public String execute(ContentManager contentManager) {
		HashMap<String, String> parameterMap = contentManager
				.getRequestParameters();
		HashMap<String, Object> requestAttributes = new HashMap<String, Object>();

		if (ExemplarStatus.FREE.toString().equals(
				parameterMap.get(EXEMPLAR_STATUS_PARAMETER))) {
			int id = Integer.parseInt(parameterMap.get(ID_PARAMETER));
			int bookId = Integer.parseInt(parameterMap.get(BOOK_ID_PARAMETER));

			if (DeleteLogic.deleteExemplar(id, bookId)) {
				requestAttributes.put(SUCCESS_KEY, true);
			} else {
				requestAttributes.put(ERROR_KEY, true);
			}
		} else {
			requestAttributes.put(HAS_SUBSCRIPTIONS_ERROR_KEY, true);
		}
		String page = PageManager.getPageName(PAGE_NAME);
		contentManager.setRequestAttributes(requestAttributes);
		return page;
	}
}
