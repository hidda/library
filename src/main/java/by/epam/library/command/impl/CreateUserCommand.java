package by.epam.library.command.impl;

import java.util.HashMap;

import by.epam.library.command.ContentManager;
import by.epam.library.command.ICommand;
import by.epam.library.entity.User;
import by.epam.library.entity.UserRole;
import by.epam.library.hash.PasswordHash;
import by.epam.library.logic.CreateUserLogic;
import by.epam.library.manager.PageManager;
import by.epam.library.validator.UserValidator;

public class CreateUserCommand implements ICommand {

	private final static String LOGIN = "login";
	private final static String PASSWORD = "password";
	private final static String FIRST_NAME = "first_name";
	private final static String LAST_NAME = "last_name";
	private final static String EMAIL = "email";
	private final static String ROLE = "role";
	private final static String LOCALE = "locale";

	private final static String CREATED = "success";
	private final static String UNIQUE = "unique";

	private final static String USER_KEY = "user";

	private final static String PAGE_NAME = "cabinet";
	private final static String FAILED_PAGE_NAME = "registration";
	private final static String SUCCESS_KEY = "registrationSuccess";
	private final static String ERROR_KEY = "errorCreateMessage";
	private final static String VALIDATION_ERROR_KEY = "validationError";
	private final static String LOGIN_NOT_UNIQUE_ERROR_KEY = "loginNotUnique";

	@Override
	public String execute(ContentManager contentManager) {
		HashMap<String, String> parameterMap = contentManager
				.getRequestParameters();

		User user = new User();
		user.setLogin(parameterMap.get(LOGIN));
		user.setPassword(PasswordHash.md5Apache(parameterMap.get(PASSWORD)));
		user.setFirstName(parameterMap.get(FIRST_NAME));
		user.setLastName(parameterMap.get(LAST_NAME));
		user.setEmail(parameterMap.get(EMAIL));
		if (parameterMap.get(ROLE) != null) {
			user.setRole(parameterMap.get(ROLE));
		} else {
			user.setRole(UserRole.READER.toString().toLowerCase());
		}

		String page = null;
		HashMap<String, Object> requestAttributes = new HashMap<String, Object>();
		UserValidator validator = new UserValidator();
		boolean isValid = validator.validate(user);
		if (isValid) {
			HashMap<String, Boolean> creationStatus = CreateUserLogic
					.createUser(user);
			boolean unique = creationStatus.get(UNIQUE);
			boolean created = creationStatus.get(CREATED);
			if (unique && created) {
				HashMap<String, Object> sessionAttributes = new HashMap<String, Object>();
				sessionAttributes.put(USER_KEY, user);
				sessionAttributes.put(LOCALE, parameterMap.get(LOCALE));
				contentManager.setSessionAttrubutes(sessionAttributes);
				requestAttributes.put(SUCCESS_KEY, true);
				page = PageManager.getPageName(PAGE_NAME);
			} else if (!unique) {
				requestAttributes.put(LOGIN_NOT_UNIQUE_ERROR_KEY, true);
				page = PageManager.getPageName(FAILED_PAGE_NAME);
			} else {
				requestAttributes.put(ERROR_KEY, true);
				page = PageManager.getPageName(FAILED_PAGE_NAME);
			}
		} else {
			requestAttributes.put(VALIDATION_ERROR_KEY, true);
			validator.setToRequest(contentManager);
			page = PageManager.getPageName(FAILED_PAGE_NAME);
		}
		contentManager.setRequestAttributes(requestAttributes);
		return page;
	}
}
