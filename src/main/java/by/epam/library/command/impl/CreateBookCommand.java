package by.epam.library.command.impl;

import java.util.HashMap;

import by.epam.library.command.ContentManager;
import by.epam.library.command.ICommand;
import by.epam.library.entity.Book;
import by.epam.library.entity.Genre;
import by.epam.library.logic.CreateBookLogic;
import by.epam.library.manager.PageManager;
import by.epam.library.validator.BookValidator;

public class CreateBookCommand implements ICommand {

	private final static String BOOK_NAME = "name";
	private final static String AUTHOR_FIRST_NAME = "first_name";
	private final static String AUTHOR_LAST_NAME = "last_name";
	private final static String PUBLISHER = "publisher";
	private final static String EDITION_YEAR = "year";
	private final static String AMOUNT = "amount";
	private final static String GENRE_ID = "genre_id";

	private final static String PAGE_NAME = "add_book";
	private final static String SUCCESS_KEY = "createSuccess";
	private final static String ERROR_KEY = "errorCreateMessage";
	private final static String VALIDATION_ERROR_KEY = "validationError";

	@Override
	public String execute(ContentManager contentManager) {

		HashMap<String, String> parameterMap = contentManager
				.getRequestParameters();

		Book book = new Book();
		book.setName(parameterMap.get(BOOK_NAME));
		book.setAuthorFistName(parameterMap.get(AUTHOR_FIRST_NAME));
		book.setAuthorLastName(parameterMap.get(AUTHOR_LAST_NAME));
		book.setPublisher(parameterMap.get(PUBLISHER));
		book.setYear(Integer.parseInt(parameterMap.get(EDITION_YEAR)));
		book.setAmount(Integer.parseInt(parameterMap.get(AMOUNT)));
		Genre genre = new Genre();
		genre.setId(Integer.parseInt(parameterMap.get(GENRE_ID)));
		book.setGenre(genre);

		HashMap<String, Object> requestAttributes = new HashMap<String, Object>();
		BookValidator validator = new BookValidator();
		boolean isValid = validator.validate(book);
		if (isValid) {
			if (CreateBookLogic.createBook(book)) {
				requestAttributes.put(SUCCESS_KEY, true);
			} else {
				requestAttributes.put(ERROR_KEY, true);
			}
		} else {
			validator.setToRequest(contentManager);
			requestAttributes.put(VALIDATION_ERROR_KEY, true);
		}
		String page = PageManager.getPageName(PAGE_NAME);
		contentManager.setRequestAttributes(requestAttributes);
		return page;
	}

}
