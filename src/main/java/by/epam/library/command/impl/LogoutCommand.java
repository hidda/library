package by.epam.library.command.impl;

import by.epam.library.command.ContentManager;
import by.epam.library.command.ICommand;
import by.epam.library.manager.PageManager;

public class LogoutCommand implements ICommand {

	private final static String PAGE_NAME = "main";

	@Override
	public String execute(ContentManager contentManager) {
		String page = PageManager.getPageName(PAGE_NAME);
		contentManager.destroySession();
		return page;
	}

}
