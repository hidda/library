package by.epam.library.command.impl;

import java.util.HashMap;

import by.epam.library.command.ContentManager;
import by.epam.library.command.ICommand;
import by.epam.library.logic.DeleteLogic;
import by.epam.library.manager.PageManager;

public class DeleteBookCommand implements ICommand {

	private final static String ID_PARAMETER = "book_id";

	private final static String HAS_UNCLOSED_SUBSCRIPTIONS = "hasUnclosed";
	private final static String DELETED = "success";

	private final static String PAGE_NAME = "books";
	private final static String SUCCESS_KEY = "deleteSuccess";
	private final static String ERROR_KEY = "errorDeleteMessage";
	private final static String HAS_SUBSCRIPTIONS_ERROR_KEY = "hasSubscriptionsError";

	@Override
	public String execute(ContentManager contentManager) {

		HashMap<String, String> parameterMap = contentManager
				.getRequestParameters();

		int id = Integer.parseInt(parameterMap.get(ID_PARAMETER));
		HashMap<String, Object> requestAttributes = new HashMap<String, Object>();
		HashMap<String, Boolean> deleteStatus = DeleteLogic.deleteBook(id);
		boolean hasSubscriptions = deleteStatus.get(HAS_UNCLOSED_SUBSCRIPTIONS);
		boolean success = deleteStatus.get(DELETED);

		if (!hasSubscriptions && success) {
			requestAttributes.put(SUCCESS_KEY, true);
		} else if (hasSubscriptions) {
			requestAttributes.put(HAS_SUBSCRIPTIONS_ERROR_KEY, true);
		} else {
			requestAttributes.put(ERROR_KEY, true);
		}
		String page = PageManager.getPageName(PAGE_NAME);
		contentManager.setRequestAttributes(requestAttributes);
		return page;
	}
}
