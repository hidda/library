package by.epam.library.command.impl;

import java.util.HashMap;

import by.epam.library.command.ContentManager;
import by.epam.library.command.ICommand;
import by.epam.library.logic.DeleteLogic;
import by.epam.library.manager.PageManager;

public class DeleteUserCommand implements ICommand {

	private static final String ID_PARAMETER = "user_id";

	private final static String HAS_UNCLOSED_SUBSCRIPTIONS = "hasUnclosed";
	private final static String DELETED = "success";

	private final static String PAGE_NAME = "main";
	private final static String FAILED_PAGE_NAME = "cabinet";
	private final static String ERROR_KEY = "errorDeleteMessage";
	private final static String HAS_SUBSCRIPTIONS_ERROR_KEY = "hasSubscriptionsError";

	@Override
	public String execute(ContentManager contentManager) {

		HashMap<String, String> parameterMap = contentManager
				.getRequestParameters();

		int id = Integer.parseInt(parameterMap.get(ID_PARAMETER));
		String page = null;
		HashMap<String, Object> requestAttributes = new HashMap<String, Object>();
		HashMap<String, Boolean> deleteStatus = DeleteLogic.deleteUser(id);
		boolean hasSubscriptions = deleteStatus.get(HAS_UNCLOSED_SUBSCRIPTIONS);
		boolean success = deleteStatus.get(DELETED);

		if (!hasSubscriptions && success) {
			page = PageManager.getPageName(PAGE_NAME);
			contentManager.destroySession();
		} else if (hasSubscriptions) {
			requestAttributes.put(HAS_SUBSCRIPTIONS_ERROR_KEY, true);
			page = PageManager.getPageName(FAILED_PAGE_NAME);
		} else {
			requestAttributes.put(ERROR_KEY, true);
			page = PageManager.getPageName(FAILED_PAGE_NAME);
		}
		contentManager.setRequestAttributes(requestAttributes);
		return page;
	}
}
