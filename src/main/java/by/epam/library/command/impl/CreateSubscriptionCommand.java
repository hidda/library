package by.epam.library.command.impl;

import java.util.HashMap;

import by.epam.library.command.ContentManager;
import by.epam.library.command.ICommand;
import by.epam.library.date.DateManager;
import by.epam.library.entity.Exemplar;
import by.epam.library.entity.Subscription;
import by.epam.library.entity.User;
import by.epam.library.logic.CreateSubscriptionLogic;
import by.epam.library.manager.PageManager;
import by.epam.library.validator.SubscriptionValidator;

public class CreateSubscriptionCommand implements ICommand {

	private final static String USER_ID = "user_id";
	private final static String EXEMPLAR_ID = "exemplar_id";
	private final static String OLD_EXEMPLAR_STATUS = "old_exemplar_status";
	private final static String SUB_END_DATE = "end_date";

	private final static String HAS_OVERDUE_SUBSCRIPTIONS = "hasOverdue";
	private final static String CREATED = "success";

	private final static String PAGE_NAME = "add_sub";
	private final static String SUCCESS_KEY = "createSuccess";
	private final static String ERROR_KEY = "errorCreateMessage";
	private final static String HAS_OVERDUE_ERROR_KEY = "hasOverdueError";
	
	@Override
	public String execute(ContentManager contentManager) {
		HashMap<String, String> parameterMap = contentManager
				.getRequestParameters();
		HashMap<String, Object> requestAttributes = new HashMap<String, Object>();

		Subscription subscription = new Subscription();
		User user = new User();
		user.setId(Integer.parseInt(parameterMap.get(USER_ID)));
		Exemplar exemplar = new Exemplar();
		exemplar.setId(Integer.parseInt(parameterMap.get(EXEMPLAR_ID)));
		exemplar.setExemplarStatus(parameterMap.get(OLD_EXEMPLAR_STATUS));
		subscription.setExemplar(exemplar);
		subscription.setUser(user);
		subscription.setStartSubcriptionDate(DateManager.getStartDate());
		if (parameterMap.get(SUB_END_DATE) != null) {
			subscription.setEndSubcriptionDate(DateManager.getDate(parameterMap
					.get(SUB_END_DATE)));
		} else {
			subscription.setEndSubcriptionDate(DateManager.autoEndSubDate());
		}
		SubscriptionValidator validator = new SubscriptionValidator();
		boolean isValid = validator.validate(subscription);
		if (isValid) {
			HashMap<String, Boolean> createStatus = CreateSubscriptionLogic
					.createSubscription(subscription);
			boolean hasOverdue = createStatus.get(HAS_OVERDUE_SUBSCRIPTIONS);
			boolean success = createStatus.get(CREATED);

			if (!hasOverdue && success) {
				requestAttributes.put(SUCCESS_KEY, true);
			} else if (hasOverdue) {
				requestAttributes.put(HAS_OVERDUE_ERROR_KEY, true);
			} else {
				requestAttributes.put(ERROR_KEY, true);
			}
		} else {
			validator.setToRequest(contentManager);
		}
		String page = PageManager.getPageName(PAGE_NAME);
		contentManager.setRequestAttributes(requestAttributes);
		return page;
	}
}
