package by.epam.library.command.impl;

import java.util.HashMap;

import by.epam.library.command.ContentManager;
import by.epam.library.command.ICommand;
import by.epam.library.logic.ChangePasswordLogic;
import by.epam.library.manager.PageManager;
import by.epam.library.validator.UserValidator;

public class ChangePasswordCommand implements ICommand {

	private final static String PASSWORD_PARAMETER = "password";
	private final static String ID_PARAMETER = "user_id";

	private final static String PAGE_NAME = "edit_profile";
	private final static String SUCCESS_KEY = "updateSuccess";
	private final static String ERROR_KEY = "updateError";
	private final static String VALIDATION_ERROR_KEY = "validationError";

	@Override
	public String execute(ContentManager contentManager) {

		HashMap<String, String> parameterMap = contentManager
				.getRequestParameters();
		HashMap<String, Object> requestAttributes = new HashMap<String, Object>();
		int userId = Integer.parseInt(parameterMap.get(ID_PARAMETER));
		String password = parameterMap.get(PASSWORD_PARAMETER);
		UserValidator validator = new UserValidator();
		boolean isValid = validator.isValid(password, PASSWORD_PARAMETER);
		if (isValid) {
			if (ChangePasswordLogic.changePassword(userId, password)) {
				requestAttributes.put(SUCCESS_KEY, true);
			} else {
				requestAttributes.put(ERROR_KEY, true);
			}
		} else {
			requestAttributes.put(VALIDATION_ERROR_KEY, true);
		}
		String page = PageManager.getPageName(PAGE_NAME);
		contentManager.setRequestAttributes(requestAttributes);
		return page;
	}

}
