package by.epam.library.command.impl;

import java.util.HashMap;

import by.epam.library.command.ContentManager;
import by.epam.library.command.ICommand;
import by.epam.library.logic.DeleteLogic;
import by.epam.library.manager.PageManager;

public class DeleteSubscriptionCommand implements ICommand {

	private final static String ID_PARAMETER = "subscription_id";

	private final static String PAGE_NAME = "subscriptions";
	private final static String SUCCESS_KEY = "deleteSuccess";
	private final static String ERROR_KEY = "errorDeleteMessage";

	@Override
	public String execute(ContentManager contentManager) {
		HashMap<String, String> parameterMap = contentManager
				.getRequestParameters();

		int id = Integer.parseInt(parameterMap.get(ID_PARAMETER));
		HashMap<String, Object> requestAttributes = new HashMap<String, Object>();

		if (DeleteLogic.deleteSubscription(id)) {
			requestAttributes.put(SUCCESS_KEY, true);
		} else {
			requestAttributes.put(ERROR_KEY, true);
		}
		String page = PageManager.getPageName(PAGE_NAME);
		contentManager.setRequestAttributes(requestAttributes);
		return page;
	}

}
