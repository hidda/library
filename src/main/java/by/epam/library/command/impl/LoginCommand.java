package by.epam.library.command.impl;

import java.util.HashMap;

import by.epam.library.command.ContentManager;
import by.epam.library.command.ICommand;
import by.epam.library.entity.User;
import by.epam.library.logic.LoginLogic;
import by.epam.library.manager.PageManager;

public class LoginCommand implements ICommand {

	private static final String LOGIN_PARAMETER = "login";
	private static final String PASSWORD_PARAMETER = "password";
	private final static String LOCALE = "locale";

	private final static String USER_KEY = "user";

	private final static String PAGE_NAME = "main";
	private final static String FAILED_PAGE_NAME = "login";
	private final static String ERROR_KEY = "errorLoginPassMessage";

	@Override
	public String execute(ContentManager contentManager) {

		HashMap<String, String> parameterMap = contentManager
				.getRequestParameters();

		String login = parameterMap.get(LOGIN_PARAMETER);
		String password = parameterMap.get(PASSWORD_PARAMETER);
		String page = null;
		User user = LoginLogic.checkLogin(login, password);

		if (user != null) {
			HashMap<String, Object> sessionAttributes = new HashMap<String, Object>();
			sessionAttributes.put(USER_KEY, user);
			sessionAttributes.put(LOCALE, parameterMap.get(LOCALE));
			contentManager.setSessionAttrubutes(sessionAttributes);
			page = PageManager.getPageName(PAGE_NAME);
		} else {
			HashMap<String, Object> requestAttributes = new HashMap<String, Object>();
			requestAttributes.put(ERROR_KEY, true);
			requestAttributes.put(LOGIN_PARAMETER, login);
			page = PageManager.getPageName(FAILED_PAGE_NAME);
			contentManager.setRequestAttributes(requestAttributes);
		}
		return page;
	}

}
