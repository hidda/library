package by.epam.library.command;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.library.entity.User;

public class ContentManager {

	private HashMap<String, String> requestParameters;
	private HashMap<String, Object> requestAttributes;
	private HashMap<String, Object> sessionAttributes;
	private HttpServletRequest httpServletRequest;

	public HashMap<String, String> getRequestParameters() {
		return requestParameters;
	}

	public void setRequestAttributes(HashMap<String, Object> requestAttributes) {
		this.requestAttributes.putAll(requestAttributes);
	}

	public void setSessionAttrubutes(HashMap<String, Object> sessionAttributes) {
		this.sessionAttributes.putAll(sessionAttributes);
	}

	public ContentManager(HttpServletRequest httpServletRequest) {
		requestParameters = new HashMap<String, String>();
		requestAttributes = new HashMap<String, Object>();
		sessionAttributes = new HashMap<String, Object>();
		this.httpServletRequest = httpServletRequest;
	}

	public void setParameters(HttpServletRequest request) {
		Enumeration<String> enumeration = request.getParameterNames();
		while (enumeration.hasMoreElements()) {
			String key = enumeration.nextElement();
			this.requestParameters.put(key, request.getParameter(key));
		}
	}

	public void putRequestAttributes(HttpServletRequest request) {

		Set<String> keySet = requestAttributes.keySet();
		Iterator<String> iterator = keySet.iterator();

		while (iterator.hasNext()) {
			String key = iterator.next();
			request.setAttribute(key, requestAttributes.get(key));
		}
	}

	public void putSessionAttributes(HttpSession session) {

		Set<String> keySet = sessionAttributes.keySet();
		Iterator<String> iterator = keySet.iterator();
		if (httpServletRequest.getSession(false) != null) {
			while (iterator.hasNext()) {
				String key = iterator.next();
				session.setAttribute(key, sessionAttributes.get(key));
			}
		}
	}
	
	public User getSessionUser() {
		return (User) httpServletRequest.getSession().getAttribute("user");
	}

	public void destroySession() {
		httpServletRequest.getSession().invalidate();
	}
}