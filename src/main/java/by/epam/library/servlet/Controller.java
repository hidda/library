package by.epam.library.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.library.command.CommandFactory;
import by.epam.library.command.ContentManager;
import by.epam.library.command.ICommand;
import by.epam.library.load.ILoad;
import by.epam.library.load.LoadFactory;

/**
 * Servlet implementation class Controller
 */
@WebServlet("/controller")
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Controller() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * This method handles all requests, defines and executes all necessary load
	 * operations, defines and executes specified command. With the
	 * ContentManager object it also sets all attributes to the request and
	 * forwards it.
	 * 
	 * @param HttpServletRequest
	 *            request
	 * @param HttpServletResponse
	 *            response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String com = request.getParameter("command");
		String[] load = request.getParameterValues("load");
		ContentManager contentManager = new ContentManager(request);
		contentManager.setParameters(request);
		ICommand command = CommandFactory.createCommand(com);
		String page = command.execute(contentManager);
		if (load != null) {
			for (int i = 0; i < load.length; i++) {
				ILoad loadCommand = LoadFactory.createLoad(load[i]);
				loadCommand.load(contentManager);
			}
		}
		contentManager.putRequestAttributes(request);
		contentManager.putSessionAttributes(request.getSession());
		request.getRequestDispatcher(page).forward(request, response);
	}

}
