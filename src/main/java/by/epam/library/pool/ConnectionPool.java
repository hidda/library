package by.epam.library.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import by.epam.library.exception.PoolException;

public class ConnectionPool {

	private final static Logger LOG = Logger.getLogger(ConnectionPool.class);
	private final static String CONFIG_NAME = "db";

	private BlockingQueue<Connection> pool;
	private ResourceBundle bundle;
	private String url;
	private String user;
	private String pass;
	private int poolSize;

	private ConnectionPool() {
		try {
			this.bundle = ResourceBundle.getBundle(CONFIG_NAME);
			this.url = bundle.getString("db.url");
			this.user = bundle.getString("db.user");
			this.pass = bundle.getString("db.password");
			this.poolSize = Integer.parseInt(bundle.getString("db.size"));
			this.pool = new LinkedBlockingQueue<Connection>(poolSize);
			buildPool();
		} catch (PoolException e) {
			LOG.fatal("Unable to create pool: " + e.getMessage() + "; "
					+ e.getCause());
			throw new ExceptionInInitializerError();
		} catch (ClassNotFoundException e) {
			LOG.error("Unable to register db-driver: " + e);
			throw new ExceptionInInitializerError();
		}
	}

	private static class PoolHolder {
		private final static ConnectionPool INSTANCE = new ConnectionPool();
	}

	public static ConnectionPool getInstance() {
		return PoolHolder.INSTANCE;
	}

	private void buildPool() throws PoolException, ClassNotFoundException {
		Class.forName(bundle.getString("db.driver"));
		for (int i = 0; i < poolSize; i++) {
			createConnection();
		}
	}

	private void createConnection() throws PoolException {
		try {
			Connection con = DriverManager.getConnection(url, user, pass);
			this.pool.offer(con);
		} catch (SQLException e) {
			throw new PoolException("Unable to create connection", e);
		}
	}

	public Connection takeConnection() throws PoolException {
		try {
			return this.pool.take();
		} catch (InterruptedException e) {
			throw new PoolException("Unable to take connection from pool: " + e);
		}
	}

	public void returnConnection(Connection con) {
		if (con != null) {
			this.pool.offer(con);
		}
	}

	public void closeAll() throws PoolException {
		for (Connection con : this.pool) {
			try {
				if (con != null && !con.isClosed()) {
					con.close();
				}
			} catch (SQLException e) {
				throw new PoolException("Unable to close connection: " + e);
			}
		}
	}
}
