package by.epam.library.validator;

import by.epam.library.entity.Book;

public class BookValidator extends AbstractValidator<Book> {

	private final static String NAME_REGEX_KEY = "name";
	private final static String BOOK_NAME_REGEX_KEY = "book_name";
	private final static String YEAR_REGEX_KEY = "year";
	private final static String PUBLISHER_REGEX_KEY = "publisher";
	private final static String AMOUNT_REGEX_KEY = "amount";

	private final static String BOOK_NAME = "name";
	private final static String AUTHOR_FIRST_NAME = "firstName";
	private final static String AUTHOR_LAST_NAME = "lastName";
	private final static String PUBLISHER = "publisher";
	private final static String EDITION_YEAR = "year";
	private final static String AMOUNT = "amount";

	@Override
	public boolean validate(Book book) {
		checkVariable(book.getAuthorFistName(), AUTHOR_FIRST_NAME, NAME_REGEX_KEY);
		checkVariable(book.getAuthorLastName(), AUTHOR_LAST_NAME, NAME_REGEX_KEY);
		checkVariable(book.getName(), BOOK_NAME, BOOK_NAME_REGEX_KEY);
		checkVariable(book.getPublisher(), PUBLISHER, PUBLISHER_REGEX_KEY);
		checkVariable(String.valueOf(book.getYear()), EDITION_YEAR, YEAR_REGEX_KEY);
		checkVariable(String.valueOf(book.getAmount()), AMOUNT, AMOUNT_REGEX_KEY);
		boolean bookValid = getValidationErrors().isEmpty();
		return bookValid;
	}
}
