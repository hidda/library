package by.epam.library.validator;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import by.epam.library.command.ContentManager;
import by.epam.library.entity.Entity;
import by.epam.library.manager.RegExManager;

/**
 * Abstract class for data validation and handling correct and incorrect values
 * 
 * @author Hidda
 *
 * @param Implementations
 *            must parameterize this class with any Entity object.
 */
public abstract class AbstractValidator<T extends Entity> {

	private final static String NOT_VALID_POSTFIX = "NotValid";

	private HashMap<String, Object> validationErrors = new HashMap<String, Object>();
	private HashMap<String, Object> validationCorrects = new HashMap<String, Object>();

	public HashMap<String, Object> getValidationErrors() {
		return validationErrors;
	}

	public HashMap<String, Object> getValidationCorrects() {
		return validationCorrects;
	}

	/**
	 * General method that validates variables from specified Entity class. Must
	 * be implemented in any class that extends this one and consist of the
	 * number of validate() operation for any variable that needs validation.
	 * 
	 * @param Entity
	 *            object specified in class that extends this one and implements
	 *            this method
	 * @return Must return general validation status: true if all variables are
	 *         correct, otherwise false.
	 */
	abstract public boolean validate(T entity);

	/**
	 * Validates String variable with the corresponding regular expression. At
	 * first, checks variable for null. If not null, checks it with regular
	 * expression. If result is the same as input parameter value, this variable
	 * is considered as valid and is set to the map with correct values.
	 * Otherwise variable goes to the map with incorrect values, that consists
	 * of String keys (variable name + postfix) and booleans with true-values
	 * (to set appropriate error message in jsp page)
	 * 
	 * @param variable
	 *            : variable to validate
	 * @param variableName
	 *            : variable's name, needed to set correct keys to maps
	 * @param regexKey
	 *            : String name for the regex.properties file
	 */
	protected void checkVariable(String variable, String variableName,
			String regexKey) {

		if (variable != null) {
			Pattern pattern = Pattern.compile(RegExManager
					.getPageName(regexKey));
			Matcher matcher = pattern.matcher(variable);
			if (matcher.find()) {
				if (variable.equals(matcher.group())) {
					putValid(variable, variableName);
				} else {
					putError(variableName);
				}
			} else {
				putError(variableName);
			}
		} else {
			putError(variableName);
		}
	}

	/**
	 * Simple method that returns boolean result of the validation in
	 * case if you need to validate not the whole object, but only one or
	 * several variables
	 * 
	 * @return boolean result of the validation: true if variable matches the
	 *         regular expression and false if it doesn't
	 *
	 * @param same
	 *            as in checkValid method
	 * @see #checkVariable(variable, variableName, regexKey)
	 */
	public final boolean isValid(String variable, String regexKey) {

		boolean isValid = false;
		if (variable != null) {
			Pattern pattern = Pattern.compile(RegExManager
					.getPageName(regexKey));
			Matcher matcher = pattern.matcher(variable);
			if (matcher.find()) {
				if (variable.equals(matcher.group())) {
					isValid = true;
				}
			}
		}
		return isValid;
	}

	/**
	 * Sets error parameter key to corresponding map. Key consists of variable
	 * name and constant postfix
	 * 
	 * @param parameterName
	 *            : variable name
	 */
	protected void putError(String parameterName) {
		String parameterKey = parameterName + NOT_VALID_POSTFIX;
		validationErrors.put(parameterKey, true);
	}

	/**
	 * Sets valid variable to corresponding map
	 * 
	 * @param parameter
	 *            : valid String variable
	 * @param parameterName
	 *            : variable name
	 */
	protected void putValid(String parameter, String parameterName) {
		validationCorrects.put(parameterName, parameter);
	}

	/**
	 * In case if general valid status is false, you may set valid variables and
	 * invalid errors to the request with the content manager object to: 1)
	 * return valid variables to the form as values; 2) set corresponding
	 * validation error message to every possible key to show user what data
	 * isn't correct
	 * 
	 * @param contentManager
	 *            : object than handles request and session content
	 */
	public void setToRequest(ContentManager contentManager) {
		contentManager.setRequestAttributes(validationCorrects);
		contentManager.setRequestAttributes(validationErrors);
	}
}
