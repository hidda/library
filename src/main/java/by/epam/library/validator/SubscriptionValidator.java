package by.epam.library.validator;

import by.epam.library.date.DateManager;
import by.epam.library.entity.ExemplarStatus;
import by.epam.library.entity.Subscription;

public class SubscriptionValidator extends AbstractValidator<Subscription> {

	private final static String STATUS = "exemplarStatus";
	private final static String END_DATE = "endSubscriptionDate";

	@Override
	public boolean validate(Subscription subscription) {
		boolean statusValid = ExemplarStatus.FREE.equals(subscription
				.getExemplar().getExemplarStatus());
		if (statusValid) {
			subscription.getExemplar().setExemplarStatus(
					ExemplarStatus.TAKEN.toString());
		} else {
			putError(STATUS);
		}
		boolean endDateValid = !DateManager.checkEndDate(subscription
				.getEndSubcriptionDate());
		if (!endDateValid) {
			putError(END_DATE);
		}
		boolean valid = getValidationErrors().isEmpty();
		return valid;
	}
}
