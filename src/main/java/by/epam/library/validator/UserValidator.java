package by.epam.library.validator;

import by.epam.library.entity.User;

public class UserValidator extends AbstractValidator<User> {

	private final static String LOGIN_REGEX_KEY = "login";
	private final static String PASSWORD_REGEX_KEY = "password";
	private final static String NAME_REGEX_KEY = "name";
	private final static String EMAIL_REGEX_KEY = "email";

	private final static String LOGIN = "login";
	private final static String PASSWORD = "password";
	private final static String FIRST_NAME = "firstName";
	private final static String LAST_NAME = "lastName";
	private final static String EMAIL = "email";

	@Override
	public boolean validate(User user) {
		checkVariable(user.getLogin(), LOGIN, LOGIN_REGEX_KEY);
		checkVariable(user.getPassword(), PASSWORD, PASSWORD_REGEX_KEY);
		checkVariable(user.getFirstName(), FIRST_NAME, NAME_REGEX_KEY);
		checkVariable(user.getLastName(), LAST_NAME, NAME_REGEX_KEY);
		checkVariable(user.getEmail(), EMAIL, EMAIL_REGEX_KEY);
		boolean userValid = getValidationErrors().isEmpty();
		return userValid;
	}

}
