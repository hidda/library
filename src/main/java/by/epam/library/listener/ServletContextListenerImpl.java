package by.epam.library.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import by.epam.library.exception.PoolException;
import by.epam.library.pool.ConnectionPool;

/**
 * Application Lifecycle Listener implementation class
 * ServletContextListenerImpl
 *
 */
@WebListener
public class ServletContextListenerImpl implements ServletContextListener {
	private final static Logger LOG = Logger
			.getLogger(ServletContextListenerImpl.class);

	/**
	 * Default constructor.
	 */
	public ServletContextListenerImpl() {
	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent sce) {
		ConnectionPool pool = ConnectionPool.getInstance();
		try {
			pool.closeAll();
		} catch (PoolException e) {
			LOG.error("Unable to close pool: " + e.getMessage());
		}
	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent sce) {
		ServletContext context = sce.getServletContext();
		System.setProperty("rootPath", context.getRealPath("/"));
		new DOMConfigurator().doConfigure(System.getProperty("rootPath")
				+ "log4j.xml", LogManager.getLoggerRepository());
	}

}
