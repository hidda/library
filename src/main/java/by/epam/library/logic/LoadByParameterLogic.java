package by.epam.library.logic;

import java.util.List;

import org.apache.log4j.Logger;

import by.epam.library.dao.BookDAO;
import by.epam.library.dao.ExemplarDAO;
import by.epam.library.dao.GenreDAO;
import by.epam.library.dao.SubscriptionDAO;
import by.epam.library.dao.UserDAO;
import by.epam.library.entity.Book;
import by.epam.library.entity.Exemplar;
import by.epam.library.entity.Genre;
import by.epam.library.entity.Subscription;
import by.epam.library.entity.User;
import by.epam.library.exception.DAOException;

public class LoadByParameterLogic {

	private final static Logger LOG = Logger
			.getLogger(LoadByParameterLogic.class);

	public static Exemplar loadExemplar(int id) {
		ExemplarDAO exemplarDAO = null;
		Exemplar exemplar = null;

		try {
			exemplarDAO = new ExemplarDAO();
			exemplar = exemplarDAO.findById(id);
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			exemplarDAO.releaseConnection();
		}
		return exemplar;
	}

	public static List<Exemplar> loadBookExemplars(int bookId) {

		List<Exemplar> exemplars = null;
		BookDAO bookDAO = null;
		ExemplarDAO exemplarDAO = null;

		try {
			bookDAO = new BookDAO();
			Book book = bookDAO.findById(bookId);
			if (book != null) {
				exemplarDAO = new ExemplarDAO(bookDAO.getConnection());
				exemplars = exemplarDAO.loadByBook(book);
			}
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			bookDAO.releaseConnection();
		}

		return exemplars;
	}

	public static List<Subscription> loadSubscriptions(int userId) {

		List<Subscription> subscriptions = null;
		UserDAO userDAO = null;
		SubscriptionDAO subscriptionDAO = null;

		try {
			userDAO = new UserDAO();
			User user = userDAO.findById(userId);
			if (user != null) {
				subscriptionDAO = new SubscriptionDAO(userDAO.getConnection());
				subscriptions = subscriptionDAO.loadByUser(user);
				if (!updateUserSubcriptions(subscriptions, subscriptionDAO)) {
					subscriptions = null;
				}
			}
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			userDAO.releaseConnection();
		}
		return subscriptions;
	}

	private static boolean updateUserSubcriptions(
			List<Subscription> subscriptions, SubscriptionDAO subscriptionDAO) {
		boolean success = subscriptionDAO.updateOverdue(subscriptions);
		return success;
	}

	public static List<Book> loadBooks(int genreId) {

		List<Book> books = null;
		GenreDAO genreDAO = null;
		BookDAO bookDAO = null;

		try {
			genreDAO = new GenreDAO();
			Genre genre = genreDAO.findById(genreId);
			if (genre != null) {
				bookDAO = new BookDAO(genreDAO.getConnection());
				books = bookDAO.loadByGenre(genre);
			}
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			genreDAO.releaseConnection();
		}
		return books;

	}

}
