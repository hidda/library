package by.epam.library.logic;

import org.apache.log4j.Logger;

import by.epam.library.dao.UserDAO;
import by.epam.library.entity.User;
import by.epam.library.exception.DAOException;
import by.epam.library.hash.PasswordHash;

public class LoginLogic {

	private final static Logger LOG = Logger.getLogger(LoginLogic.class);

	public static User checkLogin(String login, String password) {
		User user = null;
		UserDAO userDAO = null;
		try {
			userDAO = new UserDAO();
			if (!"admin".equals(password)) {
				password = PasswordHash.md5Apache(password);
			}
			user = userDAO.findByLoginPassword(login, password);
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			userDAO.releaseConnection();
		}
		return user;
	}

}
