package by.epam.library.logic;

import org.apache.log4j.Logger;

import by.epam.library.dao.ExemplarDAO;
import by.epam.library.dao.SubscriptionDAO;
import by.epam.library.entity.ExemplarStatus;
import by.epam.library.exception.DAOException;

public class UpdateLogic {

	private final static Logger LOG = Logger.getLogger(UpdateLogic.class);

	public static boolean closeSubscription(int subscriptionId, int exemplarId) {

		boolean success = true;
		SubscriptionDAO subscriptionDAO = null;
		ExemplarDAO exemplarDAO = null;

		try {
			subscriptionDAO = new SubscriptionDAO();
			success = subscriptionDAO.updateClosed(subscriptionId);
			if (success) {
				exemplarDAO = new ExemplarDAO(subscriptionDAO.getConnection());
				success = exemplarDAO.updateStatus(exemplarId, ExemplarStatus.FREE);
			}
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			subscriptionDAO.releaseConnection();
		}
		return success;
	}

}
