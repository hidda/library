package by.epam.library.logic;

import org.apache.log4j.Logger;

import by.epam.library.dao.BookDAO;
import by.epam.library.dao.ExemplarDAO;
import by.epam.library.entity.Book;
import by.epam.library.exception.DAOException;

public class CreateBookLogic {

	private final static Logger LOG = Logger.getLogger(CreateBookLogic.class);

	public static boolean createBook(Book book) {

		BookDAO bookDAO = null;
		ExemplarDAO exemplarDAO = null;
		boolean success = false;

		try {
			bookDAO = new BookDAO();
			success = bookDAO.create(book);
			if (success) {
				exemplarDAO = new ExemplarDAO(bookDAO.getConnection());
				success = exemplarDAO.create(book);
			}
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			bookDAO.releaseConnection();
		}
		return success;
	}
}
