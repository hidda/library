package by.epam.library.logic;

import java.util.HashMap;

import org.apache.log4j.Logger;

import by.epam.library.dao.ExemplarDAO;
import by.epam.library.dao.SubscriptionDAO;
import by.epam.library.entity.Subscription;
import by.epam.library.exception.DAOException;

public class CreateSubscriptionLogic {

	private final static Logger LOG = Logger
			.getLogger(CreateSubscriptionLogic.class);
	
	private final static String HAS_OVERDUE_SUBSCRIPTIONS = "hasOverdue";
	private final static String CREATED = "success";

	public static HashMap<String, Boolean> createSubscription(
			Subscription subscription) {

		SubscriptionDAO subscriptionDAO = null;
		HashMap<String, Boolean> createStatus = new HashMap<String, Boolean>(2);
		boolean success = false;
		boolean hasOverdue = false;

		try {
			subscriptionDAO = new SubscriptionDAO();
			hasOverdue = subscriptionDAO.checkUserSubscriptions(
					subscription.getUser().getId()).get(HAS_OVERDUE_SUBSCRIPTIONS);
			if (!hasOverdue) {
				if (subscriptionDAO.create(subscription)) {
					success = updateExemplar(subscriptionDAO, subscription);
				}
			}
			createStatus.put(HAS_OVERDUE_SUBSCRIPTIONS, hasOverdue);
			createStatus.put(CREATED, success);
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			subscriptionDAO.releaseConnection();
		}
		return createStatus;
	}

	private static boolean updateExemplar(SubscriptionDAO subscriptionDAO,
			Subscription subscription) {
		boolean success = false;
		ExemplarDAO exemplarDAO = new ExemplarDAO(
				subscriptionDAO.getConnection());
		success = exemplarDAO.updateStatus(subscription.getExemplar().getId(),
				subscription.getExemplar().getExemplarStatus());
		return success;
	}
}
