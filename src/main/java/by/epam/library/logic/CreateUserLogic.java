package by.epam.library.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.library.dao.UserDAO;
import by.epam.library.entity.User;
import by.epam.library.exception.DAOException;

public class CreateUserLogic {

	private final static Logger LOG = Logger.getLogger(CreateUserLogic.class);
	
	private final static String SUCCESS_KEY = "success";
	private final static String UNIQUE_KEY = "unique";

	public static HashMap<String, Boolean> createUser(User user) {

		HashMap<String, Boolean> creationStatus = new HashMap<String, Boolean>();
		boolean success = false;
		boolean unique = false;
		UserDAO userDAO = null;

		try {
			userDAO = new UserDAO();
			unique = checkLogin(userDAO, user.getLogin());
			if (unique) {
				success = userDAO.create(user);
			}
			creationStatus.put(UNIQUE_KEY, unique);
			creationStatus.put(SUCCESS_KEY, success);
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			userDAO.releaseConnection();
		}
		return creationStatus;
	}

	private static boolean checkLogin(UserDAO userDAO, String login) {

		List<String> logins = new ArrayList<String>();
		logins = userDAO.loadLogins();
		boolean unique = true;

		for (String log : logins) {
			if (login.equals(log)) {
				unique = false;
			}
		}
		return unique;
	}
}
