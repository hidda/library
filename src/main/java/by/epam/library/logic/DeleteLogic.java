package by.epam.library.logic;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.library.dao.BookDAO;
import by.epam.library.dao.ExemplarDAO;
import by.epam.library.dao.SubscriptionDAO;
import by.epam.library.dao.UserDAO;
import by.epam.library.entity.Book;
import by.epam.library.entity.Exemplar;
import by.epam.library.entity.ExemplarStatus;
import by.epam.library.exception.DAOException;

public class DeleteLogic {

	private final static Logger LOG = Logger.getLogger(DeleteLogic.class);
	
	private final static String HAS_UNCLOSED_KEY = "hasUnclosed";
	private final static String SUCCESS_KEY = "success";

	public static boolean deleteExemplar(int id, int bookId) {

		boolean success = false;
		ExemplarDAO exemplarDAO = null;

		try {
			exemplarDAO = new ExemplarDAO();
			SubscriptionDAO subscriptionDAO = new SubscriptionDAO(
					exemplarDAO.getConnection());
			success = subscriptionDAO.deleteByExemplar(id);
			if (success) {
				success = exemplarDAO.deleteById(id);
				if (success) {
					success = decreaseBookAmount(exemplarDAO, bookId);
				}
			}
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			exemplarDAO.releaseConnection();
		}
		return success;
	}

	private static boolean decreaseBookAmount(ExemplarDAO exemplarDAO,
			int bookId) {

		BookDAO bookDAO = null;
		boolean success = false;

		bookDAO = new BookDAO(exemplarDAO.getConnection());
		Book book = bookDAO.findById(bookId);
		if (book != null) {
			success = bookDAO.decreaseNumber(book);
		}
		return success;
	}

	public static HashMap<String, Boolean> deleteBook(int id) {

		HashMap<String, Boolean> deleteStatus = new HashMap<String, Boolean>();
		boolean hasOpenSubscriptinos = false;
		boolean success = false;
		BookDAO bookDAO = null;

		try {
			bookDAO = new BookDAO();
			Book book = bookDAO.findById(id);
			ExemplarDAO exemplarDAO = new ExemplarDAO(bookDAO.getConnection());
			deleteStatus = deleteExemplarsByBook(exemplarDAO, book,
					deleteStatus);
			hasOpenSubscriptinos = deleteStatus.get(HAS_UNCLOSED_KEY);
			success = deleteStatus.get(SUCCESS_KEY);
			if (success && !hasOpenSubscriptinos) {
				success = bookDAO.deleteById(id);
				deleteStatus.put(SUCCESS_KEY, success);
			}
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			bookDAO.releaseConnection();
		}
		return deleteStatus;
	}

	private static HashMap<String, Boolean> deleteExemplarsByBook(
			ExemplarDAO exemplarDAO, Book book,
			HashMap<String, Boolean> deleteStatus) {

		boolean hasOpenSubscriptinos = false;
		boolean success = false;
		SubscriptionDAO subscriptionDAO = new SubscriptionDAO(
				exemplarDAO.getConnection());

		List<Exemplar> exemplars = exemplarDAO.loadByBook(book);
		for (Exemplar exemplar : exemplars) {
			if (ExemplarStatus.FREE.equals(exemplar.getExemplarStatus())) {
				success = subscriptionDAO.deleteByExemplar(exemplar.getId());
				if (success) {
					success = exemplarDAO.deleteById(exemplar.getId());
				}
			} else {
				hasOpenSubscriptinos = true;
				break;
			}
		}
		deleteStatus.put(HAS_UNCLOSED_KEY, hasOpenSubscriptinos);
		deleteStatus.put(SUCCESS_KEY, success);
		return deleteStatus;
	}

	public static HashMap<String, Boolean> deleteUser(int id) {

		HashMap<String, Boolean> deleteStatus = new HashMap<String, Boolean>();
		boolean hasOpenSubscriptinos = false;
		boolean success = false;
		UserDAO userDAO = null;

		try {
			userDAO = new UserDAO();
			SubscriptionDAO subscriptionDAO = new SubscriptionDAO(
					userDAO.getConnection());
			hasOpenSubscriptinos = subscriptionDAO.checkUserSubscriptions(id).get(HAS_UNCLOSED_KEY);
			if (!hasOpenSubscriptinos) {
				if (subscriptionDAO.deleteByUser(id)) {
					success = userDAO.deleteById(id);
				}
			}
			deleteStatus.put(HAS_UNCLOSED_KEY, hasOpenSubscriptinos);
			deleteStatus.put(SUCCESS_KEY, success);
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			userDAO.releaseConnection();
		}
		return deleteStatus;
	}

	public static boolean deleteSubscription(int id) {

		boolean success = false;
		SubscriptionDAO subscriptionDAO = null;

		try {
			subscriptionDAO = new SubscriptionDAO();
			success = subscriptionDAO.deleteById(id);
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			subscriptionDAO.releaseConnection();
		}
		return success;
	}
}
