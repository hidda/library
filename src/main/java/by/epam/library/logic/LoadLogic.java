package by.epam.library.logic;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.library.dao.BookDAO;
import by.epam.library.dao.ExemplarDAO;
import by.epam.library.dao.GenreDAO;
import by.epam.library.dao.SubscriptionDAO;
import by.epam.library.dao.UserDAO;
import by.epam.library.entity.Book;
import by.epam.library.entity.Exemplar;
import by.epam.library.entity.Genre;
import by.epam.library.entity.Subscription;
import by.epam.library.entity.User;
import by.epam.library.exception.DAOException;

public class LoadLogic {

	private final static Logger LOG = Logger.getLogger(LoadLogic.class);

	public static List<Genre> loadAllGenres() {
		List<Genre> allGenres = null;
		GenreDAO genreDAO = null;
		try {
			genreDAO = new GenreDAO();
			allGenres = genreDAO.loadAll();
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			genreDAO.releaseConnection();
		}
		return allGenres;
	}

	public static List<Exemplar> loadAllExemplars() {
		List<Exemplar> allExemplars = null;
		ExemplarDAO exemplarDAO = null;
		try {
			exemplarDAO = new ExemplarDAO();
			allExemplars = exemplarDAO.loadAll();
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			exemplarDAO.releaseConnection();
		}
		return allExemplars;
	}

	public static List<Book> loadAllBooks() {
		List<Book> allBooks = new ArrayList<Book>();
		BookDAO bookDAO = null;
		try {
			bookDAO = new BookDAO();
			allBooks = bookDAO.loadAll();
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			bookDAO.releaseConnection();
		}
		return allBooks;
	}

	public static List<Subscription> loadAllSubscriptions() {
		List<Subscription> allSubscriptions = null;
		SubscriptionDAO subscriptionDAO = null;
		try {
			subscriptionDAO = new SubscriptionDAO();
			allSubscriptions = subscriptionDAO.loadAll();
			if (allSubscriptions != null && !allSubscriptions.isEmpty()) {
				boolean success = subscriptionDAO
						.updateOverdue(allSubscriptions);
				if (!success) {
					allSubscriptions = null;
				}
			}
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			subscriptionDAO.releaseConnection();
		}
		return allSubscriptions;
	}

	public static List<User> loadUserInfo() {

		List<User> usersInfo = null;
		UserDAO userDAO = null;

		try {
			userDAO = new UserDAO();
			usersInfo = userDAO.loadUserInfo();
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			userDAO.releaseConnection();
		}
		return usersInfo;
	}

}
