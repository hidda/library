package by.epam.library.logic;

import org.apache.log4j.Logger;

import by.epam.library.dao.UserDAO;
import by.epam.library.exception.DAOException;
import by.epam.library.hash.PasswordHash;

public class ChangePasswordLogic {

	private final static Logger LOG = Logger
			.getLogger(ChangePasswordLogic.class);

	public static boolean checkPassword(String password, int userId) {

		boolean success = false;
		UserDAO userDAO = null;

		try {
			userDAO = new UserDAO();
			if (!"admin".equals(password)) {
				password = PasswordHash.md5Apache(password);
			}
			success = userDAO.checkPassword(password, userId);
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			userDAO.releaseConnection();
		}
		return success;
	}

	public static boolean changePassword(int userId, String password) {

		boolean success = false;
		UserDAO userDAO = null;

		try {
			userDAO = new UserDAO();
			password = PasswordHash.md5Apache(password);
			success = userDAO.changePassword(userId, password);
		} catch (DAOException e) {
			LOG.error(e.getMessage());
		} finally {
			userDAO.releaseConnection();
		}
		return success;
	}

}
