package by.epam.library.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

public class FooterTag extends TagSupport {
	
	private final static Logger LOG = Logger.getLogger(FooterTag.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public int doStartTag() throws JspException {
		String startTag = "<footer class=\"footer\">";
		String copyright = "Copyright: Hidda, 2015";
		String br = "<br />";
		String contacts = "Contacts: the.white.noise.00@gmail.com";
		String endTag = "</footer>";
		try {
			pageContext.getOut().write(startTag + copyright + br + contacts + endTag);
		} catch (IOException e) {
			LOG.error("Footer tag error occured: " + e);
		}
		return SKIP_BODY;
	}

}
