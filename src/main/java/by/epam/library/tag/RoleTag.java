package by.epam.library.tag;

import javax.servlet.jsp.tagext.TagSupport;

import by.epam.library.entity.User;
import by.epam.library.entity.UserRole;

public class RoleTag extends TagSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String role;

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public int doStartTag() {

		User user = (User) pageContext.getSession().getAttribute("user");

		if (user == null) {
			pageContext.getRequest().setAttribute("guest", true);
		} else if (UserRole.ADMIN.equals(user.getRole())){
			pageContext.getRequest().setAttribute("admin", true);
		}
		else {
			pageContext.getRequest().setAttribute("reader", true);
		}

		return SKIP_BODY;
	}

}
