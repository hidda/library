-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.41-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных library
DROP DATABASE IF EXISTS `library`;
CREATE DATABASE IF NOT EXISTS `library` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `library`;


-- Дамп структуры для таблица library.book
DROP TABLE IF EXISTS `book`;
CREATE TABLE IF NOT EXISTS `book` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `author_first_name` varchar(100) NOT NULL,
  `author_last_name` varchar(100) NOT NULL,
  `publisher` varchar(100) NOT NULL,
  `year` int(4) NOT NULL,
  `amount` int(11) NOT NULL DEFAULT '1',
  `genre_id` int(11) NOT NULL,
  PRIMARY KEY (`book_id`),
  KEY `FK_book_genre` (`genre_id`),
  CONSTRAINT `FK_book_genre` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`genre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы library.book: ~11 rows (приблизительно)
DELETE FROM `book`;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` (`book_id`, `name`, `author_first_name`, `author_last_name`, `publisher`, `year`, `amount`, `genre_id`) VALUES
	(5, 'Jonathan Strange & Mr Norrell', 'Susanna', 'Clarke', 'Bloomsbury', 2004, 4, 7),
	(6, 'The Hobbit', 'John R. R.', 'Tolkien', 'Houghton Mifflin Harcourt', 2012, 3, 7),
	(7, 'Открытки с того света', 'Франко', 'Армино', 'Ad Marginem', 2013, 2, 19),
	(8, 'Начинается ночь', 'Майкл', 'Каннингем', 'CORPUS', 2011, 3, 4),
	(9, 'Зелень. Трава. Благодать.', 'Шон', 'Макбрайд', 'Издательский дом Флюид', 2008, 4, 16),
	(10, 'Monstrous Regiment', 'Терри', 'Пратчетт', 'Эксмо', 2013, 2, 7),
	(11, 'Занимательное волноведение', 'Гэвин', 'Претор-Пинни', 'Лайвбук', 2012, 5, 32),
	(12, 'Игра престолов', 'Джордж', 'Мартин', 'АСТ', 2001, 2, 7),
	(13, '100 уважительных причин незамедлительно покончить с собой', 'Ролан', 'Топор', 'Опустошитель', 2014, 3, 4),
	(14, 'Как посадить Airbus A330', 'Джеймс', 'Мэй', 'Эксмо', 2011, 1, 31),
	(15, 'Евклидово окно', 'Леонард', 'Млодинов', 'Лайвбук', 2013, 2, 32);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;


-- Дамп структуры для таблица library.exemplar
DROP TABLE IF EXISTS `exemplar`;
CREATE TABLE IF NOT EXISTS `exemplar` (
  `exemplar_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `status` enum('free','library_room','taken') NOT NULL,
  PRIMARY KEY (`exemplar_id`),
  KEY `FK_exemplar_book` (`book_id`),
  CONSTRAINT `FK_exemplar_book` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы library.exemplar: ~31 rows (приблизительно)
DELETE FROM `exemplar`;
/*!40000 ALTER TABLE `exemplar` DISABLE KEYS */;
INSERT INTO `exemplar` (`exemplar_id`, `book_id`, `status`) VALUES
	(12, 5, 'taken'),
	(13, 5, 'taken'),
	(14, 5, 'free'),
	(15, 5, 'free'),
	(16, 6, 'free'),
	(17, 6, 'taken'),
	(18, 6, 'taken'),
	(19, 7, 'free'),
	(20, 7, 'free'),
	(21, 8, 'free'),
	(22, 8, 'free'),
	(23, 8, 'free'),
	(24, 9, 'taken'),
	(25, 9, 'free'),
	(26, 9, 'free'),
	(27, 9, 'free'),
	(28, 10, 'free'),
	(29, 10, 'free'),
	(30, 11, 'taken'),
	(31, 11, 'free'),
	(32, 11, 'free'),
	(33, 11, 'free'),
	(34, 11, 'free'),
	(35, 12, 'free'),
	(36, 12, 'free'),
	(37, 13, 'free'),
	(38, 13, 'free'),
	(39, 13, 'free'),
	(40, 14, 'free'),
	(41, 15, 'free'),
	(42, 15, 'free');
/*!40000 ALTER TABLE `exemplar` ENABLE KEYS */;


-- Дамп структуры для таблица library.genre
DROP TABLE IF EXISTS `genre`;
CREATE TABLE IF NOT EXISTS `genre` (
  `genre_id` int(11) NOT NULL AUTO_INCREMENT,
  `genre` enum('fiction','nonfiction') DEFAULT 'fiction',
  `subgenre` varchar(50) NOT NULL,
  PRIMARY KEY (`genre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы library.genre: ~34 rows (приблизительно)
DELETE FROM `genre`;
/*!40000 ALTER TABLE `genre` DISABLE KEYS */;
INSERT INTO `genre` (`genre_id`, `genre`, `subgenre`) VALUES
	(1, 'fiction', 'Adventure'),
	(2, 'fiction', 'Comic/Graphic Novel'),
	(3, 'fiction', 'Crime/Detective'),
	(4, 'fiction', 'Drama'),
	(5, 'fiction', 'Fable'),
	(6, 'fiction', 'Fairy Tale'),
	(7, 'fiction', 'Fantasy'),
	(8, 'fiction', 'Folklore'),
	(9, 'fiction', 'Historical Fiction'),
	(10, 'fiction', 'Horror'),
	(11, 'fiction', 'Humor'),
	(12, 'fiction', 'Legend'),
	(13, 'fiction', 'Magical Realism'),
	(14, 'fiction', 'Mystery'),
	(15, 'fiction', 'Mythology'),
	(16, 'fiction', 'Realistic Fiction'),
	(17, 'fiction', 'Poetry'),
	(18, 'fiction', 'Science Fiction'),
	(19, 'fiction', 'Short Story'),
	(20, 'fiction', 'Suspense/Thriller'),
	(21, 'fiction', 'Comedy'),
	(22, 'fiction', 'Satire'),
	(23, 'fiction', 'Tall Tale'),
	(24, 'fiction', 'Western'),
	(25, 'fiction', 'Romance'),
	(26, 'nonfiction', 'Biography/Autobiography'),
	(27, 'nonfiction', 'Essay'),
	(28, 'nonfiction', 'Narrative nonfiction'),
	(29, 'nonfiction', 'Speech'),
	(30, 'nonfiction', 'Monograph'),
	(31, 'nonfiction', 'Textbook'),
	(32, 'nonfiction', 'Popular Science'),
	(33, 'nonfiction', 'Travel'),
	(34, 'nonfiction', 'Reference Book');
/*!40000 ALTER TABLE `genre` ENABLE KEYS */;


-- Дамп структуры для таблица library.subscription
DROP TABLE IF EXISTS `subscription`;
CREATE TABLE IF NOT EXISTS `subscription` (
  `subscription_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `exemplar_id` int(11) NOT NULL,
  `sub_start_date` date NOT NULL,
  `sub_end_date` date NOT NULL,
  `overdue` enum('Y','N') NOT NULL DEFAULT 'N',
  `closed` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`subscription_id`),
  KEY `FK_subscription_exemplar` (`exemplar_id`),
  KEY `FK_subscription_user` (`user_id`),
  CONSTRAINT `FK_subscription_exemplar` FOREIGN KEY (`exemplar_id`) REFERENCES `exemplar` (`exemplar_id`),
  CONSTRAINT `FK_subscription_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы library.subscription: ~10 rows (приблизительно)
DELETE FROM `subscription`;
/*!40000 ALTER TABLE `subscription` DISABLE KEYS */;
INSERT INTO `subscription` (`subscription_id`, `user_id`, `exemplar_id`, `sub_start_date`, `sub_end_date`, `overdue`, `closed`) VALUES
	(5, 3, 12, '2015-08-27', '2015-09-09', 'N', 'N'),
	(9, 3, 18, '2015-08-27', '2015-09-27', 'N', 'Y'),
	(10, 5, 13, '2015-08-28', '2015-09-28', 'N', 'N'),
	(11, 5, 17, '2015-08-28', '2015-09-28', 'N', 'N'),
	(12, 5, 18, '2015-08-28', '2015-08-27', 'N', 'Y'),
	(13, 5, 15, '2015-08-28', '2015-08-29', 'N', 'Y'),
	(14, 5, 18, '2015-08-28', '2015-08-27', 'Y', 'N'),
	(15, 3, 14, '2015-08-30', '2015-08-28', 'N', 'Y'),
	(16, 3, 16, '2015-08-30', '2015-08-27', 'N', 'Y'),
	(18, 9, 30, '2015-08-31', '2015-09-30', 'N', 'N'),
	(19, 9, 24, '2015-08-31', '2015-09-06', 'N', 'N');
/*!40000 ALTER TABLE `subscription` ENABLE KEYS */;


-- Дамп структуры для таблица library.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `role` enum('admin','reader') NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы library.user: ~4 rows (приблизительно)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `login`, `password`, `first_name`, `last_name`, `email`, `role`) VALUES
	(1, 'admin', 'admin', 'Jane', 'Doe', 'admin@admin.com', 'admin'),
	(3, 'hidda', 'a6c97eb244b3cdb5406732111903207c', 'Polina', 'Kubashina', 'the.white.noise.00@gmail.com', 'reader'),
	(5, 'test', '76738fb79893fa4f9fe7abfa13f15d37', 'Test', 'User', 'test@test.ru', 'reader'),
	(9, 'cecil_palmer', 'bc1ccb967d4b8278f57caefb8811f206', 'Cecil Gershwin', 'Palmer', 'cecil@night.vale', 'reader');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
